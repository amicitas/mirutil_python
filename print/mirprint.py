# -*- coding: utf-8 -*-
"""
author
------
  Novimir Antoniuk Pablant
    - npablant@pppl.gov
    - novimir.pablant@amicitas.com

"""
import numpy as np

def mirprint(obj, **kwargs):
    string = mirstring(obj, **kwargs)
    print(string)


def mirstring(obj, **kwargs):
    if isinstance(obj, dict):
        string = _mirstring_dict(obj, **kwargs)
    elif isinstance(obj, np.ndarray):
        string = _mirstring_array(obj, **kwargs)
    else:
        string = str(obj)
        
    return string

def _mirstring_dict(obj, level=0, compact=True):
    string = ''
    for key in obj:
        string +='{}{}'.format('  '*level,key)
        if isinstance(obj[key], dict):
            string += '\n'
            string += _mirstring_dict(obj[key], level=level+1)
        else:
            # Here we format and print the dict value.
            if isinstance(obj[key], np.ndarray):
                obj_repr= '{}'.format(
                    _mirstring_array(obj[key], compact=compact))
            else:
                obj_repr= '{}'.format(obj[key])
                
            string += ':  {}\n'.format(obj_repr)
            
    return string

def _mirstring_array(array, numformat=None, compact=False):
    """
    Create a string for an ndarray.

    Warning:
      This will not produce correct output for multi-dimensional arrays.
    """

    if array.ndim == 0:
        return str(array)
    
    if compact:
        num_to_print = min(array.shape[-1], 4)

        index = [0 for xx in range(array.ndim)]
        index[-1] = slice(None, num_to_print)
    else:
        index = np.ones(array.shape, dtype=bool)

    if numformat is None:
        if np.isreal(array.flat[0]):
            mask = array.flat[:num_to_print] != 0
            if np.sum(mask) == 0:
                numformat = '6.2f'
            else:
                if ((np.min(np.abs(array.flat[:num_to_print][mask])) > 1e-2)
                    and (np.max(np.abs(array.flat[:num_to_print][mask])) < 1e3)):
                    numformat = '6.2f'
                else:
                    numformat = '9.2e'
        else:
            numformat = ''
    
    numfmt_str = '{:'+numformat+'}'
    fmt = '['*array.ndim
    fmt += ', '.join([numfmt_str for xx in range(num_to_print)])
    if num_to_print < len(array):
        fmt += ', ...'
    fmt += ']'
    if array.ndim > 1:
        fmt+= ' ...]'*(array.ndim-1)
    
    string = fmt.format(*array.flat[:num_to_print])

    return string
