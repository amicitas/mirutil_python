# -*- coding: utf-8 -*-
"""
author
------
  Novimir Antoniuk Pablant
    - npablant@pppl.gov
    - novimir.pablant@amicitas.com

"""

import re
from collections.abc import Iterable

def _rep_sub(m, arglen):
    """Create the substitute string based on the match object."""
    
    # Here we figure out if the repetition number was explicit or implicit.
    if m.group(1):
        repnum = int(m.group(1))
        exp = False
    else:
        repnum = arglen
        exp = True
        
    sub = m.group(3).join((m.group(2) for xx in range(repnum)))
    return sub, exp
        
    
def _rep_expand(fmt, arglens):
    """Find the repetition patterns and replace them."""
    
    # This will match both explicit and implicit repitition patters.
    pattern = r'\{\{\*(\d*)\|(.*?)\|(.*?)\}\}'
    new = []
    expand = []
    for ii, m in enumerate(re.finditer(pattern, fmt)):
        if arglens:
            exp_str, exp_flag = _rep_sub(m, arglens[ii])
        else:
            exp_str, exp_flag = _rep_sub(m, None)
        new.append(exp_str)
        expand.append(exp_flag)
    
    for sub in new:
        fmt = re.sub(r'\{\{\*(\d*)\|(.*?)\|(.*?)\}\}', sub, fmt, count=1)

    return fmt, expand


def mirformat(fmt, *args):
    """
    String formatting that supports a repitition syntax and support for 
    iterables. 

    The format syntax is identical to str.format() with the additon of the 
    repitition directive: {{*N|P|S}}
    

    function syntax
    ---------------

    mirformat(format_string, *args)


    format string syntax
    --------------------

    A repitition block is specified with:
      {{*N|P|S}}

    N: Number of repititions
    P: String to be repeated
    S: Separator

    N,P and S are all optional. If N is absent the number of repetitions will be
    automatically generated based on the input lengths.


    examples
    --------

    mirformat('{{*2|string|, }}')
    > string, string

    mirformat('{{*2|string|}}')
    > stringstring

    mirformat('{{*2|{:0.4f}|, }}', [1.0, 2.0])
    > 1.0000, 2.0000, 3.0000


    mirformat('{{*|{:0.4f}|, }}', [1.0, 2.0, 3.0])
    > 1.0000, 2.0000, 3.0000


    mirformat('{{*2|{:0.4f}|, }}, {:0.1f}', [1.0, 2.0, 3.0])
    > 1.0000, 2.0000, 3.0
    """
    
    # Make a list of argument lengths, as needed for implicit expansion.
    arglens = []
    for a in args:
        if isinstance(a, str):
            arglens.append(1)         
        elif isinstance(a, Iterable):
            arglens.append(len(a))
        else:
            arglens.append(1)
                               
    fmt, exp = _rep_expand(fmt, arglens)
    
    #print('new_fmt:')
    #print(fmt)
    
    # If implicit expansion was found, end expand the argument.
    arglist = []
    for a in args:
        if isinstance(a, str):
            arglist.append(a)         
        elif exp and isinstance(a, Iterable):
            for aa in a:
                arglist.append(aa)
        else:
            arglist.append(a)
            
    # Do the final expansion.
    output = fmt.format(*arglist)
    
    return output
