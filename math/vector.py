
import numpy as np

def normalize(vector):
    """
    Return a normalized vector.
    """
    return vector/magnitude(vector)


def magnitude(vector):
    """
    Return the magnitude of the given vector.
    """
    
    return np.sqrt((vector*vector).sum(axis=0))    
