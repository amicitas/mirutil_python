
import numpy as np
from collections import namedtuple


def _cubicSplineSingle(x, knots_x, knots_y, deriv, extrapolate=False):
    
    # Choose the right section.
    w = np.where(x >= knots_x)[0]
    if w.size == 0:
        raise Exception('input value is outside of range defined by knots.')

    ii = w[-1]

    # It's OK if x is on the last knot.
    if ii == knots_x.size-1:
        ii = ii - 1

    a = deriv[ii]*(knots_x[ii+1] - knots_x[ii]) - (knots_y[ii+1] - knots_y[ii])
    b = -1*deriv[ii+1]*(knots_x[ii+1] - knots_x[ii]) + (knots_y[ii+1] - knots_y[ii])
    t = (x - knots_x[ii])/(knots_x[ii+1] - knots_x[ii])

    y = (1 - t)*knots_y[ii] + t*knots_y[ii+1] + t*(1-t)*(a*(1-t)+b*t)

    return y
    
def cubicSpline(x, knots_x_in, knots_y_in, deriv_in, extrapolate=False):
    """
    A simple definition of a cubic spline.
    """

    # First make sure the knots are in order
    idx_sort = knots_x_in.argsort()
    
    knots_x = knots_x_in[idx_sort]
    knots_y = knots_y_in[idx_sort]
    deriv = deriv_in[idx_sort]

    # Check if any of the input values are out of range.

    if np.isscalar(x):
        return _cubicSplineSingle(x,  knots_x, knots_y, deriv, extrapolate=extrapolate)
    else:
        return np.array([_cubicSplineSingle(v,  knots_x, knots_y, deriv, extrapolate=extrapolate) for v in x])

## def _quadraticSplineSingle(x, knots_x, knots_y, deriv, extrapolate=False):
    
##     # Choose the right section.
##     w = np.where(x >= knots_x)[0]
##     if w.size == 0:
##         ii = 0
##     else:
##         ii = w[-1]

##     # It's OK if x is on the last knot.
##     if ii == knots_x.size-1:
##         ii = ii - 1

##     a = knots_y[ii]
##     b = deriv[ii]
##     c = (deriv[ii+1] - deriv[ii])/2
    
##     t = (x - knots_x[ii])/(knots_x[ii+1] - knots_x[ii])

##     y = a + b*t + c*t**2

##     return y


## QuadraticSplineResult = namedtuple('QuadraticSplineResult', ['y', 'd', 'd2'])
## def quadraticSpline(x, knots_x_in, knots_y_in, extrapolate=False, derivatives=None):
##     """
##     Evaluate a quadratic spline.
##     """

##     # First make sure the knots are in order
##     idx_sort = knots_x_in.argsort()
    
##     knots_x = knots_x_in[idx_sort]
##     knots_y = knots_y_in[idx_sort]

##     # determine the derivatives.
##     left = np.zeros((knots_x.size, knots_x.size))
##     right = np.zeros((knots_x.size))
##     for ii in range(knots_x.size-1):
##         left[ii,ii:ii+2] = 1 
##         right[ii] = 2*(knots_y[ii+1] - knots_y[ii])

##     # Setup a boundry condition.
##     # Here I am setting the derivative to zero at the first knot.
##     left[-1,0] = 1
##     right[-1] = 0
    
##     deriv = np.linalg.solve(left, right)

##     # Check if any of the input values are out of range.

    
##     if np.isscalar(x):
##         result = _quadraticSplineSingle(x,  knots_x, knots_y, deriv, extrapolate=extrapolate)
##     else:
##         result = np.array([_quadraticSplineSingle(v,  knots_x, knots_y, deriv, extrapolate=extrapolate)
##                            for v in x])

##     return QuadraticSplineResult(result, deriv, None)


def _quadraticSplineSection(x, knots_x):
    # Choose the right section.
    w = np.where(x >= knots_x)[0]
    if w.size == 0:
        ii = 0
    else:
        ii = w[-1]

    # It's OK if x is on the last knot.
    if ii == knots_x.size-1:
        ii = ii - 1

    return ii

def _quadraticSplineY(x, knots_x, knots_y, deriv, extrapolate=False):
    
    ii = _quadraticSplineSection(x, knots_x)

    a = knots_y[ii]
    b = deriv[ii]*(knots_x[ii+1] - knots_x[ii])
    c = (deriv[ii+1] - deriv[ii])/2.0*(knots_x[ii+1] - knots_x[ii])
    
    t = (x - knots_x[ii])/(knots_x[ii+1] - knots_x[ii])

    y = a + b*t + c*t**2

    return y

def _quadraticSplineD2(x, knots_x, knots_y, deriv, extrapolate=False):
    
    ii = _quadraticSplineSection(x, knots_x)

    return (deriv[ii+1] - deriv[ii])/(knots_x[ii+1] - knots_x[ii])


QuadraticSplineResult = namedtuple('QuadraticSplineResult', ['y', 'd', 'd2'])
def quadraticSpline(x, knots_x_in, knots_y_in, d0=0.0, extrapolate=False, derivatives=None):
    """
    Evaluate a quadratic spline.
    """

    # First make sure the knots are in order
    idx_sort = knots_x_in.argsort()
    
    knots_x = knots_x_in[idx_sort]
    knots_y = knots_y_in[idx_sort]

    # determine the derivatives.
    left = np.zeros((knots_x.size, knots_x.size))
    right = np.zeros((knots_x.size))
    for ii in range(knots_x.size-1):
        left[ii,ii] = 1.0
        left[ii,ii+1] = 1.0 
        right[ii] = 2*(knots_y[ii+1] - knots_y[ii])/(knots_x[ii+1] - knots_x[ii])

    # Setup a boundry condition.
    # Here I am setting the derivative to zero at the first knot.
    left[-1,0] = 1.0
    right[-1] = d0
    
    deriv = np.linalg.solve(left, right)

    # Check if any of the input values are out of range.


    # Calculate the y value and the second derivative.
    if np.isscalar(x):
        y = _quadraticSplineY(x,  knots_x, knots_y, deriv, extrapolate=extrapolate)
    else:
        y = np.array([_quadraticSplineY(xx,  knots_x, knots_y, deriv, extrapolate=extrapolate) for xx in x])

    # Calculate the second derivative at the knot locations.
    # This is not efficent, as I am recalculating the spline section unecessarily.
    d2 = np.array([_quadraticSplineD2(xx,  knots_x, knots_y, deriv, extrapolate=extrapolate) for xx in knots_x])
        
    return QuadraticSplineResult(y, deriv, d2)
