#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
"""
package mirpython

author
------
  Novimir Antoniuk Pablant
    - npablant@pppl.gov
    - novimir.pablant@amicitas.com

description
-----------
  Evaluate a gaussian

"""
# ------------------------------------------------------------------------------

import numpy as np

def gaussian(x, parameters=None, intensity=None, location=None, sigma=None):

    if parameters is None:
        if (intensity is None) or (location is None) or (sigma is None):
            raise Exception('Required gaussian parmameters were not givin as inputs.')
        
        parameters=np.array([intensity, location, sigma])

        
    return parameters[0]/parameters[2]/np.sqrt(2.0*np.pi)*np.exp(-0.5*((x - parameters[1])/parameters[2])**2)
