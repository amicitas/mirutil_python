# -*- coding: utf-8 -*-
"""
author
------
  Novimir Antoniuk Pablant
    - npablant@pppl.gov
    - novimir.pablant@amicitas.com

description
-----------
  Some utilities based on mpmath.

  For now this only includes the voight cdf implemenation
  which is too slow to use in most cases.  This may however
  be useful if I ever need to generate a psudo-function.
"""

from mpmath import mp, fp

def voigt_cdf(x, gamma, sigma):
    """
    When using mpmath.mp this works very well, however it is 
    extremely slow. The mpmath.fp version is fast, but is not 
    stable or usable.
    """
    z = (x + 1j*gamma)/mp.sqrt(2)/sigma
    y = 0.5 + mp.erf(z)/2.0 + 1j*z**2/mp.pi*mp.hyp2f2(1, 1, 3/2, 2, -1*z**2)
    y = y.real
    return np.float64(y)

voigt_cdf_np = np.frompyfunc(voigt_cdf, 3, 1)
