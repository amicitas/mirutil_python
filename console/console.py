# -*- coding: utf-8 -*-
#*****************************************************************************
#       Copyright (C) 2009 Novimir Pablant
#       Copyright (C) 2003-2006 Gary Bishop.
#       Copyright (C) 2006  Jorgen Stenarson. <jorgen.stenarson@bostream.nu>
#
#  Distributed under the terms of the BSD License.  The full license is in
#  the file COPYING, distributed as part of this software.
#
#  Portions were modeled after the winconsole C extension by Fredrik Lundh.
#*****************************************************************************
#  Version: 1.0.0
#
#  Changelog:
#  2015-06-15: 1.0.0 - Novimir Pablant
#    Added a version number to help tracking accross projects.
#
#*****************************************************************************
'''Color control for the Windows and Posix consoles.

Allows the use of html tags to use for coloring.
'''
from __future__ import print_function
import re
import os

########################################################################
########################################################################
## SETUP GENERAL ROUTINES
########################################################################
def getConsole():
    """
    Will return a console appropriate for the os.
    """
    if os.name == 'nt':
        return WinConsole()
    elif os.name == 'posix':
        return AnsiConsole()
    else:
        print('Console not avalable for "{0}"'.format(os.name))


########################################################################
########################################################################
## SETUP MAIN CONSOLE CLASS.
## 
########################################################################

class Console(object):
    '''Console driver.  This is the base class for WinConsole and AnsiConsole.'''

    def __init__(self):
        '''Initialize the Console object.'''
        
        # Setup the Regular Expressions
        self.setupRegex()

        # Setup the colors
        self.setupColors()

    def __del__(self):
        pass

    def setupRegex(self):
        """
        Setup the regular expressions to match html color tags 
        and ansi escape sequences.
        """

        # Match the ansi escape sequences for color.
        self.regex_ansi_escape = re.compile('(?P<full>\001?\033\\[(?P<codes>[0-9;]+)m\002?)')

        # Setup for HTML tags
        
        # This will match any html tag <tag></tag>
        self.regex_html = re.compile(r"""(?P<full>\<(?P<tag>\w+)(?P<attributes>.*?)\>(?P<text>.*?)\</(?P=tag).*?\>)""")

        # This will match only the console html tag <console></console>
        self.regex_html_console = re.compile(r"""(?P<full>\<(?P<tag>console)(?P<attributes>.*?)\>(?P<text>.*?)\</(?P=tag).*?\>)""")

        # This will match <font color="color"></font> tags.
        self.regex_html_attr_color = re.compile(r""".*(?<= )(color=(?P<quote>\"|\')(?P<color>.*?)(?P=quote))""")

        # This will match a css color style specification.
        #   style"color:color"
        self.regex_html_css_color = re.compile(r""".*(?<= )(style=(?P<quote>\"|\').*?(?<=[ ;"'])color:(?P<color>\w*).*?(?P=quote))""")



    def setupColors(self):
        """
        Setup the color tables to go between html, ansi and windows.
        """
        self.win_html_colors = { 'grey': 0x1+0x2+0x4
                                 ,'red': 0x4+0x8
                                 ,'green': 0x2+0x8
                                 ,'yellow': 0x4+0x2+0x8
                                 ,'blue': 0x1+0x8
                                 ,'purple': 0x1+0x4+0x8
                                 ,'cyan': 0x1+0x2+0x8
                                 ,'white': 0x1+0x2+0x4+0x8
                                 ,'black': 0x0
                                 ,'darkgrey':0x1+0x2+0x4
                                 ,'darkred': 0x4
                                 ,'darkgreen': 0x2
                                 ,'darkyellow': 0x4+0x2
                                 ,'darkblue': 0x1
                                 ,'darkpurple': 0x1+0x4
                                 ,'darkcyan': 0x2+0x4
                                 }

        self.ansi_html_colors = { 'grey': '1;37'
                                  ,'red': '1;31'
                                  ,'green': '1;32'
                                  ,'yellow': '1;33'
                                  ,'blue': '1;34'
                                  ,'purple': '1;35'
                                  ,'cyan': '1;36'
                                  ,'white': '1;37'
                                  ,'black': '30'
                                  ,'darkgrey': '1;30'
                                  ,'darkred': '31'
                                  ,'darkgreen': '32'
                                  ,'darkyellow': '33'
                                  ,'darkblue': '34'
                                  ,'darkpurple': '35'
                                  ,'darkcyan': '36'
                                  }

        self.ansi_to_html_bold = { '30': 'grey'
                                  ,'31': 'red'
                                  ,'32': 'green'
                                  ,'33': 'yellow'
                                  ,'34': 'blue'
                                  ,'35': 'purple'
                                  ,'36': 'cyan'
                                  ,'37': 'white'
                                  }
        self.ansi_to_html = { '30': 'black'
                             ,'31': 'darkred'
                             ,'32': 'darkgreen'
                             ,'33': 'darkyellow'
                             ,'34': 'darkblue'
                             ,'35': 'darkpurple'
                             ,'36': 'darkcyan'
                             ,'37': 'grey'
                             }

        self.ansi_to_win_bold = { '30': 0x1+0x2+0x4       #dark gray
                                  ,'31': 0x4+0x8          #red
                                  ,'32': 0x2+0x8          #light green
                                  ,'33': 0x4+0x2+0x8      #yellow
                                  ,'34': 0x1+0x8          #light blue
                                  ,'35': 0x1+0x4+0x8      #light purple
                                  ,'36': 0x1+0x2+0x8      #light cyan
                                  ,'37': 0x1+0x2+0x4+0x8  #white
                                  }
        self.ansi_to_win = { '30': 0x0              #black
                             ,'31': 0x4             #red
                             ,'32': 0x2             #green
                             ,'33': 0x4+0x2         #brown?
                             ,'34': 0x1             #blue
                             ,'35': 0x1+0x4         #purple
                             ,'36': 0x2+0x4         #cyan
                             ,'37': 0x1+0x2+0x4     #grey
                             }

    def htmlToAnsi(self, text, alltags=False):
        """
        Convert HTML color to ansi escapes
        
        By default this only looks for <console></console> tags.

        If the keyword alltags=True then all html tags with color
        specifications are converted.

        This routine cannot deal with nested tags.
        """
        search_position = 0

        # Set up the opening and closing ansi color sequences.
        ansi_open = '\033[{0}m'
        ansi_close = '\033[0m'

        while True:
            color = ''
            if alltags:
                # Match any html tag.
                match = self.regex_html.search(text, search_position)
            else:
                # Match only <console></console> tags.
                match = self.regex_html_console.search(text, search_position)
        
            if not match:
                # No more escape sequences found.
                break

            span_full = match.span('full')
            search_position = span_full[1]

            # First try to match a css color.  If no css expresion is found,
            # then search for a font tag with a color attribute. <font color=color>.
            match_style = self.regex_html_css_color.search(match.group('attributes'))
            if match_style:
                color = match_style.group('color')
            else:
                if match.group('tag') == 'font':
                    match_font = self.regex_html_attr_color.search(match.group('attributes'))
                    if match_font:
                        color = match_font.group('color')

            # If a tag with a color spec is found, then see if it can
            # be matched to any of the ansi colors.
            if color:
                try:
                    ansicolor = self.ansi_html_colors[color]
                except KeyError:
                    ansicolor = None

            # If a match was found, then do the subsitution.
            if ansicolor:
                ansi_color_esc = ansi_open.format(ansicolor)
                text = text[:span_full[0]] + ansi_color_esc \
                    + match.group('text') + ansi_close + text[span_full[1]:]

                search_position += len(ansi_color_esc) + len(ansi_close) \
                    + len(match.group('text')) - (span_full[1] - span_full[0])
            
        return text
                             
    def ansiToHtml(self, text, usespan=False):
        """
        Convert all ansi escape color sequences into HTML.
        
        <console style"color:color;"></console> will be used
        to specify color.

        If the keyword usespan=True then a span tag will be
        used instead of the console tag.
        """
        is_tag_open = False
        search_position = 0

        if usespan:
            tag_close = '</span>'
            tag_open = '<span style="color:{0};">'
        else:
            tag_close = '</console>'
            tag_open = '<console style="color:{0};">'

        while True:
            color = ''
            # Match an ansi color escape.
            match = self.regex_ansi_escape.search(text, search_position)

            if not match:
                # No more escape sequences found.
                break

            span_full =  match.span('full')
            search_position = span_full[1]

            # We found an ansi escape sequence.
            if match.group('codes'):
                codes = match.group('codes').split(';')
                if codes == ['0']:
                    # Found the reset flag.
                    if is_tag_open:
                        text = text[:span_full[0]] + tag_close + text[span_full[1]:]
                        search_position += len(tag_close) - (span_full[1]-span_full[0])
                        is_tag_open = False
                        continue
                elif '1' in codes:
                    # Bold color
                    for code in codes:
                        if code in self.ansi_to_html_bold.iterkeys():
                            color =  self.ansi_to_html_bold[code]
                else:
                    # Normal color
                    for code in codes:
                        if code in self.ansi_to_html.iterkeys():
                            color =  self.ansi_to_html[code]
                
                if color:
                    # Found a color.
                    tag = tag_open.format(color)
                    text = text[:span_full[0]]+tag+text[span_full[1]:]
                    search_position += len(tag) - (span_full[1]-span_full[0])

                    # Add a close tag if the previous tag was not closed.
                    if is_tag_open:
                        text = text[:span_full[0]] + tag_close + text[span_full[0]:]
                        search_position += len(tag_close)

                    # Mark the tag as open.
                    is_tag_open = True

        if is_tag_open:
            # Make sure all tags are closed.
            text += tag_close

        return text



########################################################################
########################################################################
## SETUP ANSI CONSOLE
########################################################################
class AnsiConsole(Console):
    '''Console driver for ansi terminals.'''

    def __init__(self):
        '''
        Initialize the AnsiConsole object.
        '''
        
        # First initialize the console class.
        Console.__init__(self)

    def printColor(self, *args, **kwargs):
        """
        Print to ansi terminal with colors.
        
        Keywords:
          html = True
            Convert html colors to ansi before printing.
          alltags = False
            Convert all html tags with color specifications
            to ansi.  By default only <console></console>
            tags are used.
        """

        # Check for the 'html' and 'alltags' keywords, 
        # then remove from the keyword dict.
        if ('html' in kwargs):
            html =  kwargs['html']
            del kwargs['html']
        else:
            html = True

        if ('alltags' in kwargs):
            alltags =  kwargs['alltags']
            del kwargs['alltags']
        else:
            alltags = False

        if html:
            args = map(lambda x: self.htmlToAnsi(x, alltags=alltags), args)

        print(*args, **kwargs)

########################################################################
########################################################################
## SETUP WINDOWS CONSOLE
########################################################################
if os.name == 'nt':
    try:
        from ctypes import *
        from _ctypes import call_function
    except ImportError:
        raise ImportError("You need ctypes to run this code")


    # some constants we need
    STD_INPUT_HANDLE = -10
    STD_OUTPUT_HANDLE = -11

    WHITE = 0x7
    BLACK = 0


    # Windows structures we'll need later
    class COORD(Structure):
        _fields_ = [("X", c_short),
                    ("Y", c_short)]

    class SMALL_RECT(Structure):
        _fields_ = [("Left", c_short),
                    ("Top", c_short),
                    ("Right", c_short),
                    ("Bottom", c_short)]

    class CONSOLE_SCREEN_BUFFER_INFO(Structure):
        _fields_ = [("dwSize", COORD),
                    ("dwCursorPosition", COORD),
                    ("wAttributes", c_short),
                    ("srWindow", SMALL_RECT),
                    ("dwMaximumWindowSize", COORD)]

    # I didn't want to have to individually import these so I made a list, they are
    # added to the Console class later in this file.
    funcs = [
        'AllocConsole',
        'CreateConsoleScreenBuffer',
        'FillConsoleOutputAttribute',
        'FillConsoleOutputCharacterW',
        'FreeConsole',
        'GetConsoleCursorInfo',
        'GetConsoleMode',
        'GetConsoleScreenBufferInfo',
        'GetConsoleTitleW',
        'GetProcAddress',
        'GetStdHandle',
        'PeekConsoleInputW',
        'ReadConsoleInputW',
        'ScrollConsoleScreenBufferW',
        'SetConsoleActiveScreenBuffer',
        'SetConsoleCursorInfo',
        'SetConsoleCursorPosition',
        'SetConsoleMode',
        'SetConsoleScreenBufferSize',
        'SetConsoleTextAttribute',
        'SetConsoleTitleW',
        'SetConsoleWindowInfo',
        'WriteConsoleW',
        'WriteConsoleOutputCharacterW',
        'WriteFile',
        ]

    class WinConsole(Console):
        '''Console driver for Windows.'''

        def __init__(self):
            '''
            Initialize the WinConsole object.
            '''

            # First initialize the console class.
            Console.__init__(self)

            # Setup the standard input
            self.hout = self.GetStdHandle(STD_OUTPUT_HANDLE)

            # Get the current colors
            info = CONSOLE_SCREEN_BUFFER_INFO()
            self.GetConsoleScreenBufferInfo(self.hout, byref(info))

            # remember the initial colors
            self.saveattr = info.wAttributes 

            # Combine the escape colors with the background.
            background = self.saveattr & 0xf0

            for escape in self.ansi_to_win_bold:
                if self.ansi_to_win_bold[escape] is not None:
                    self.ansi_to_win_bold[escape] |= background

            for escape in self.ansi_to_win:
                if self.ansi_to_win[escape] is not None:
                    self.ansi_to_win[escape] |= background

            for escape in self.win_html_colors:
                if self.win_html_colors[escape] is not None:
                    self.win_html_colors[escape] |= background

        def __del__(self):
            '''Cleanup the console when finished.'''
            self.SetConsoleTextAttribute(self.hout, self.saveattr)
            Console.__del__(self)


        def printWinAttr(self, *args, **kwargs):
            '''
            print_win_attr(object, attr=None, end='\n', sep='')

            Applies the attributs in the keyword 'attr' to the console.
            Sends the object to print().
            Resets the console to the original value.
            '''
            self.SetConsoleTextAttribute(self.hout, kwargs['attr'])
            # Remove the 'attr' keyword, pass everything else to print()
            del kwargs['attr']
            print(*args, **kwargs)
            self.SetConsoleTextAttribute(self.hout, self.saveattr)


        def printColor(self, *args, **kwargs):
            """
            Print to windows terminal with colors.

            Keywords:
              from_html = True
                Convert html colors to windows colors.
              from_ansi = True
                Convert ansi colors to windows colors.
            """

            if ('ansi' in kwargs):
                ansi = kwargs['ansi']
                del kwargs['ansi']
            else:
                ansi = True

            if ('html' in kwargs):
                html =  kwargs['html']
                del kwargs['html']
            else:
                html = True

            if ('alltags' in kwargs):
                alltags =  kwargs['alltags']
                # Do not delete the keyword so we can pass it to
                # printColorFromHtml
            else:
                alltags = False

            if ansi and html:
                args = map(self.ansiToHtml, args)
                self.printColorFromHtml(*args, **kwargs)
            elif html:
                self.printColorFromHtml(*args, **kwargs)
            elif ansi:
                del kwargs['alltags']
                self.printColorFromAnsi(*args, **kwargs)
            else:
                del kwargs['alltags']
                print(*args, **kwargs)

        def printColorFromAnsi(self, *args, **kwargs):
            """
            Converts ansi color codes to windows console colors. And prints.
            """
            first_arg = True
            position = 0
            for arg in args:
                if not first_arg:
                    print(' ', end='')

                first_arg = False

                for match in self.regex_ansi_escape.finditer(arg):
                    print(arg[position:match.start('full')], end='')
                    position = match.end('full')
                    if match.group('codes'):
                        codes = match.group('codes').split(';')
                        if codes == ['0']:
                            self.SetConsoleTextAttribute(self.hout, self.saveattr)
                        elif '1' in codes:
                            for code in codes:
                                if code in self.ansi_to_win_bold.iterkeys():
                                    self.SetConsoleTextAttribute(self.hout
                                                                 ,self.ansi_to_win_bold[code])
                        else:
                            for code in codes:
                                if code in self.ansi_to_win.iterkeys():
                                    self.SetConsoleTextAttribute(self.hout
                                                                 ,self.ansi_to_win[code])
                print(arg[position:], end='')
            print()

            self.SetConsoleTextAttribute(self.hout, self.saveattr)

        def printColorFromHtml(self, *args, **kwargs):
            """
            Converts html color codes to windows console colors and prints.
        
            By default this only looks for <console></console> tags.

            If the keyword alltags=True then all html tags with color
            specifications are converted.

            Note: Nested tags are not supported.
            """

            # Check for the alltags keyword
            if ('alltags' in kwargs):
                alltags =  kwargs['alltags']
                del kwargs['alltags']
            else:
                alltags = False

            # Setup the keywords for print()
            if 'sep' in kwargs:
                if kwargs['sep'] == None:
                    separator = ' '
                else:
                    separator = kwargs['sep']

            if 'file' in kwargs:
                file = kwargs['file']
            else:
                file = None

            if 'end' in kwargs:
                end = kwargs['end']
            else:
                end = None


            first_arg = True
            position = 0

            for arg in args:
                # Add the separator character.
                if not first_arg:
                    print(separator, end='', file=file)

                first_arg = False

                # choose whether to look for all tags or just <console> tags.
                if alltags:
                    regex = self.regex_html
                else:
                    regex = self.regex_html_console

                for match in regex.finditer(arg):
                    # An html tag was found.
                    color = ''

                    # First try to match a css color.  If no css expresion is found,
                    # then search for a font tag with a color attribute. <font color=color>.
                    match_style = self.regex_html_css_color.search(match.group('attributes'))
                    if match_style:
                        color = match_style.group('color')
                    else:
                        if match.group('tag') == 'font':
                            match_font = self.regex_html_attr_color.search(match.group('attributes'))
                            if match_font:
                                color = match_font.group('color')

                    # If a tag with a color spec is found, then see if it can
                    # be matched to any of the windwos colors.
                    if color:
                        try:
                            wincolor = self.win_html_colors[color]
                        except KeyError:
                            wincolor = None

                    # If a match was found, then do the subsitution.
                    if wincolor:
                        # First print out the text upto the html tag.
                        print(arg[position:match.start('full')], end='', file=file)

                        # Set the color
                        self.SetConsoleTextAttribute(self.hout, wincolor)

                        # Print the text
                        print(match.group('text'), end='', file=file)

                        # Reset the color
                        self.SetConsoleTextAttribute(self.hout, self.saveattr)
                    else:
                        # No valid color found, just print out the whole tag.
                        print(arg[positon:match.end('full')], end='', file=file)

                    # Set the position to the end of the html tag.
                    position = match.end('full')

                # Print out what ever is left.
                print(arg[position:], end='', file=file)

            # Add the end character, if any.
            print(end=end, file=file)

    # add the functions from the dll to the WinConsole class
    for func in funcs:
        setattr(Console, func, getattr(windll.kernel32, func))
