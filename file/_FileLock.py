# Copyright (c) 2009, Evan Fosmark
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met: 
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer. 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution. 
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# The views and conclusions contained in the software and documentation are those
# of the authors and should not be interpreted as representing official policies, 
# either expressed or implied, of the FreeBSD Project.



"""
Implementation of a simple cross-platform file locking mechanism.

example
-------
  with FileLock("test.txt", timeout=2) as lock:
    print("Lock acquired.")
    # Do something with the locked file
  

change log
----------
  This is a modified version of code orignially written by Evan Fosmark
  The original code was released under the BSD License, as is this modified version.

  Version 3.0
    This has a few changes that Novimir Pablant made.

    - Lockfile filename is now given by the user instead of being generated.
    - No longer writes anything to the lock file.
    - Added a cleanup flag.
    
  Version 2.0
    Source taken from:
      https://github.com/ilastik/lazyflow/blob/master/lazyflow/utility/fileLock.py
  
    - Tweak docstrings for sphinx.
    - Accept an absolute path for the protected file (instead of a file name 
      relative to cwd).
    - Allow timeout to be None.
    - Fixed a bug that caused the original code to be NON-threadsafe when the same 
      FileLock instance was shared by multiple threads in one process.
      (The original was safe for multiple processes, but not multiple threads in a 
      single process.  This version is safe for both cases.)
    - Added ``purge()`` function.
    - Added ``available()`` function.
    - Expanded API to mimic ``threading.Lock interface``:
      - ``__enter__`` always calls ``acquire()``, and therefore blocks if 
        ``acquire()`` was called previously.
      - ``__exit__`` always calls ``release()``.  It is therefore a bug to call 
        ``release()`` from within a context manager.
      - Added ``locked()`` function. 
      - Added blocking parameter to ``acquire()`` method
"""

import os
import time
import errno
 
class FileLock(object):
    """ A file locking mechanism that has context-manager support so 
        you can use it in a ``with`` statement. This should be relatively cross
        compatible as it doesn't rely on ``msvcrt`` or ``fcntl`` for the locking.
    """
 
    class FileLockException(Exception):
        pass

    
    def __init__(self
                 ,lockfile=None
                 ,timeout=None
                 ,delay=0.05
                 ,cleanup=True):
        """ Prepare the file locker. Specify the file to lock and optionally
            the maximum timeout and the delay between each attempt to lock.
        """
        self.is_locked = False
        
        if lockfile is None:
            self.lockfile = '.lock'
        else:
            self.lockfile = lockfile
    
        self.timeout = timeout
        self.delay = delay
        self._cleanup = cleanup
                
    def locked(self):
        """
        Returns True iff the file is owned by THIS FileLock instance.
        (Even if this returns false, the file could be owned by another FileLock 
        instance, possibly in a different thread or process).
        """
        return self.is_locked

    
    def available(self):
        """
        Returns True iff the file is currently available to be locked.
        """
        return not os.path.exists(self.lockfile)

    
    def acquire(self, blocking=True):
        """ Acquire the lock, if possible. If the lock is in use, and `blocking` is False, return False.
            Otherwise, check again every `self.delay` seconds until it either gets the lock or
            exceeds `timeout` number of seconds, in which case it raises an exception.
        """
        start_time = time.time()
        while True:
            try:
                # Attempt to create the lockfile.
                # These flags cause os.open to raise an OSError if the file already exists.
                with os.open( self.lockfile, os.O_CREAT | os.O_EXCL | os.O_RDWR ) as f:
                    pass
                break
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise 
                if self.timeout is not None and (time.time() - start_time) >= self.timeout:
                    raise FileLock.FileLockException("Timeout occurred.")
                if not blocking:
                    return False
                time.sleep(self.delay)
        self.is_locked = True
        return True

        
    def release(self):
        """ Get rid of the lock by deleting the lockfile. 
            When working in a `with` statement, this gets automatically 
            called at the end.
        """
        os.unlink(self.lockfile)
        self.is_locked = False
        
 
    def __enter__(self):
        """ Activated when used in the with statement. 
            Should automatically acquire a lock to be used in the with block.
        """
        self.acquire()
        return self
 
 
    def __exit__(self, type, value, traceback):
        """ Activated at the end of the with statement.
            It automatically releases the lock if it isn't locked.
        """
        self.release()
 
 
    def __del__(self):
        """ Make sure this ``FileLock`` instance doesn't leave a .lock file
            lying around.
        """
        if self._cleanup and self.is_locked:
            self.release()

            
    def purge(self):
        """
        For debug purposes only.  Removes the lock file from the hard disk.
        """
        if os.path.exists(self.lockfile):
            self.release()
            return True
        return False
