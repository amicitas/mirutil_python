# -*- coding: utf-8 -*-
"""
author
------
  Novimir Antoniuk Pablant
    - npablant@pppl.gov
    - novimir.pablant@amicitas.com

"""
import numpy as np
import io
import datetime
from mirutil import logging

class MirEgdata:

    def __init__(self):
        self.log = logging.getLogger(self.__class__.__name__)

        
    def writeEgdata(self, data, filename):
        string = self._generateEgdata(data)
        with open(filename, 'w') as ff:
            ff.write(string)
            
        self.log.info('File written to: {}'.format(filename))

            
    def _generateEgdata(self, data):
        """
        Generate a standard Egdata file from a dictionary in the SignalObject format.
        """
        options = {}
        options['replace_nan'] = False
        
        param = {}
        comments = {}
        #comments = data['info']
        
        # Setup defaults
        if not 'ContactName' in comments:
            comments['ContactName'] = 'Novimir Antoniuk Pablant <npablant@pppl.gov>'

        # This is redundant for egdata files, but since this is a parameter in my
        # hdf5 output format, I will keep it here nontheless.
        if not 'FileGenerationDate' in comments:
            comments['FileGenerationDate'] = datetime.datetime.now().isoformat()

        # 'datetime is a reqired parameter that refers to when this data
        # file was generated.
        if not 'datetime' in param:
            param['datetime'] = datetime.datetime.now().strftime('%m/%d/%Y %H:%M')

        param['pointname'] = 'temp'
        param['shot'] = data['info'].get('shot', 0)
        
        param['dimno'] = len(data['dim'])
        param['dimname'] = list(data['dim'].keys())
        param['dimsize'] = [len(xx) for xx in data['dim'].values()]
        param['dimunit'] = [data['units'].get(xx, '?') for xx in data['dim']]

        param['coordno'] = len(data['coord'])
        param['coordname'] = list(data['coord'].keys())
        param['coordunit'] = [data['units'].get(xx, '?') for xx in data['coord']]
        
        param['valno'] = len(data['value'])
        param['valname'] = list(data['value'].keys())
        param['valunit'] = [data['units'].get(xx, '?') for xx in data['value']]

        param['sigmano'] = len(data['sigma'])
        param['sigmaname'] = ['sigma_'+xx for xx in data['sigma']]
        param['sigmaunit'] = [data['units'].get(xx, '?') for xx in data['sigma']]

        param['maskno'] = len(data['mask'])
        param['maskname'] = ['mask_'+xx for xx in data['mask']]
        param['maskunit'] = ['none' for xx in data['mask']]
        
        # Setup the header.
        header_template = (
            "# [Parameters]\n"
            + "# Name = {pointname}\n"
            + "# ShotNo = {shot}\n"
            + "# Date = {datetime}\n"
            + "# \n"
            + "# DimNo = {dimno}\n"
            + "# DimName = {dimname}\n"
            + "# DimSize = {dimsize}\n"
            + "# DimUnit = {dimunit}\n"
            + "# \n"
            + "# ValNo = {valno}\n"
            + "# ValName = {valname}\n"
            + "# ValUnit = {valunit}\n"
            )
        header = header_template.format(**{
            'pointname':"'"+str(param['pointname'])+"'"
            ,'shot':param['shot']
            ,'datetime':"'"+str(param['datetime'])+"'"
            ,'dimno':param['dimno']
            ,'dimname':', '.join(["'"+x+"'" for x in param['dimname']])
            ,'dimsize':', '.join([str(x) for x in param['dimsize']])
            ,'dimunit':', '.join(["'"+x+"'" for x in param['dimunit']])
            ,'valno':param['coordno']+param['valno']+param['sigmano']+param['maskno']
            ,'valname':', '.join(["'"+x+"'" for x in param['coordname']+param['valname']+param['sigmaname']+param['maskname']])
            ,'valunit':', '.join(["'"+x+"'" for x in param['coordunit']+param['valunit']+param['sigmaunit']+param['maskunit']])
            })

        header += (
            "#\n"
            + "# [Comments]\n"
            )

        # Deal with comments, but skip any notes for now.
        for key in comments:
            if key == 'Notes':
                continue

            # For now just assume that all the comments are strings.
            header += "# {} = '{}'\n".format(key, str(comments[key]))

        # Add the notes if they exist.
        if 'Notes' in comments.keys():
            header += "#\n"
            header += "# {} = '{}'\n".format('Notes', str(comments['Notes']))



        header += (
            "#\n"
            + "# [data]\n"
            )

        #print(header)

        # For nan or inf values are not generally supported in the LHD Egdata
        # file.  For now just replace them with zeros.
        #if options['replace_nan']:
        #    replacement_value = 0.0
        #    w = np.nonzero(~ np.isfinite(data.ravel()))[0]
        #    data.ravel()[w] = replacement_value


        # Do everything the same way as we would with a file object.
        filehandle = io.StringIO()
        filehandle.write(header)

        for ii in range(param['dimsize'][0]):
            for jj in range(param['dimsize'][1]):
                l = []
                for ii_dim, key in enumerate(data['dim']):
                    if ii_dim == 0:
                        l.append('{:12.5e}'.format(data['dim'][key][ii]))
                    if ii_dim == 1:
                        l.append('{:12.5e}'.format(data['dim'][key][jj]))                        
                for key in data['coord']:
                    l.append('{:12.5e}'.format(data['coord'][key][ii,jj]))                    
                for key in data['value']:
                    l.append('{:12.5e}'.format(data['value'][key][ii,jj]))
                for key in data['sigma']:
                    l.append('{:12.5e}'.format(data['sigma'][key][ii,jj]))
                for key in data['mask']:
                    l.append('{:12.5e}'.format(data['mask'][key][ii,jj]))
                line = ', '.join(l) + '\n'
                filehandle.write(line)

        output = filehandle.getvalue()
        
        return output
