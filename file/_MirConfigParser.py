#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
"""
\package mirpython

\author
   Novimir Antoniuk Pablant
     - npablant@pppl.gov
     - novimir.pablant@amicitas.com


This is an object to deal with configuration files (*.ini).

"""
# ------------------------------------------------------------------------------

import ast
import re
from collections import OrderedDict


class MirConfigParser(OrderedDict):
    """
    This is an object to deal with configuration files (*.ini).

    This is meant to have basically the same functionality as the
    built in class configparser.ConfigParser.  There a few places where
    ConfigParser did not meet my needs, therefor I decided to just roll my own.
    
    The specific reasons for building my own class are as follows:
       handling of malformed entries
       multiple values for a given entry
       automatic type conversion
    """

    comment_prefixes = ('#', ';')
    delimiters = ('=', ':')

    list_delimchar = (',',)
    list_nestchar = (('\'','\''), ('\"','\"'))


    def _strip_nesting_chars(self, string, nestchar):
        """
        Strip the outermost charaters of the given string if they match
        the nesting characters.

        \inputs
          string
              The input string.
              
          nestchar
              A list of tuples of the starting and ending characters of the
              nesting pair.  Example: [("(", ")"), ("{","}")] 
        """
        
        for nn in nestchar:
            if string[0] == nn[0] and string[-1] == nn[1]:
                return string[1:-1]
            
        return string
                    

        
    def split(self
              ,string
              ,stripwhitespace=True
              ,stripnestingchar=True
              ,delimchar=None
              ,nestchar=None):
        """
        Split a line by a delimeter, but ignore delimeters inside quotes.

        \description
    
          In particular, this will respect both single and double quotes
          (or for that matter any other type of nesting symbols, for exaxple
          parenthisis or brackets).

          As far as I can tell, python does not natively have any function
          that will work for me.  The csv and shlex come close, but neither
          can deal with my particuar need.

          csv:
            Can only deal with double quotes: "".  There is no support for
            simulteaniously dealing with both double and single quotes.

          shlex:
            This can deal with both double and single quotes, but splits
            on whitespace, not on a given delimiter.

          There is no simple way to combine the two to provide the needed
          behavior.


        \testcase
          An example string to parse (badly formatted csv):
            string = ''' 'one' , two, "three, x","four 'x'" , five "x" '''

          Desired output:
            output = ['one', 'two', 'three, x', 'four \'x\'', 'five, "x"']    
        """

        output = []
        nesting = []

        if delimchar is None: delimchar = (',',)
        if nestchar is None: nestchar = (('\'','\''), ('\"','\"'))

        current = ''
        for token in string:
            gotonext = False
            
            if len(nesting) == 0:
                # No nesting at this point.

                for nn in nestchar:
                    if token == nn[0]:
                        nesting.append(nn[1])
                        current += token
                        gotonext = True
                        continue
                if gotonext: continue
                
                for dd in delimchar:
                    if token == dd:
                        output.append(current)
                        current = ''
                        gotonext = True
                        continue
                if gotonext: continue
                
            else:
                # Currently nested.
                if token == nesting[-1]:
                    nesting.pop()
                    current += token
                    continue

            current += token
                
        if current:
            output.append(current)

        # Remove leading and trailing whitespace.
        if stripwhitespace:
            output = [str.strip(x) for x in output]
            
        # Remove outermost quotes.
        if stripnestingchar:
            output = [self._strip_nesting_chars(x, nestchar) for x in output]
            
        return output

    
    def guess_data_type(self, value_list):
        """
        Attempt to guess the data type for a list of strings.

        This may not be an efficent way to do this, but it is effective.
        This will only handle the following types:
          float, long, int, bool, str
        """

        typelist = []

        # Check the data type of each of the line.
        for value in value_list:
            try:
                current_type = type(ast.literal_eval(value))
            except:
                current_type = str

            # If we ever run into a string, immediately return.
            if current_type == str: return current_type
            
            typelist.append(current_type)

        if float in typelist:
            return float
        elif int in typelist:
            return int
        elif bool in typelist:
            return bool
        else:
            raise ValueError("Unknown data type.")
        

        
    def readFile(self
                  ,fileobj
                  ,source=None
                  ,clear=True
                  ,lowercase_sections=False
                  ,lowercase_options=False
                  ,strict=False):
        """
        Parse a configuration file object into the MirConfigParser dict.
        
        keywords:
            clear (bool): default=True
                Will clear all entries from the ConfigParser before before
                reading in the new file.
                
            lowercase_sections (bool): default=False
                Convert all the sections to lowercase.
                
            lowercase_options (bool): default = False
                Convert all option names to lowercase.
                
            strict (bool): default = False
                When False:
                - Lines that cannot be parsed will be skipped.
                - Unquoted values that cannot be converted into numbers
                  will be treated like strings.
                  
        """

        # Build the regular expressions.
        re_section = re.compile(' *\[(?P<section>[ \w]+)\]')
        re_option = re.compile('(?P<option>[ \w]+)['+''.join(self.delimiters)+'](?P<value>.*)')

        current_section = 'DEFAULT'
        
        # clear the current dictionary.
        if clear:
            self.clear()

        # Loop through all the lines and start parsing.
        for line in fileobj:

            line = line.strip()
            
            # Skip any empty lines.
            if not line.strip():
                continue
            
            # Skip any lines that start with the comment character.
            # Note that spaces before the comment character are ignored.
            for char in self.comment_prefixes:
                if line[0] == char:
                    continue

            
            # Check to see if this is a section header.
            match = re_section.match(line)
            if match:
                current_section = match.group('section').strip()
                
                if lowercase_sections:
                    current_section = current_section.lower()

                # Create the section if does not exist already.
                if not current_section in self:
                    self[current_section] = OrderedDict()

                continue

            # now look for option matches.
            match = re_option.match(line)
            if match:
                
                # Create the section if it does not exist already.
                # This is only here to create the default section if needed.
                if not current_section in self:
                    self[current_section] = OrderedDict()
                    
                option = match.group('option').strip()
                value = match.group('value').strip()
                
                if lowercase_options:
                    option = option.lower()

                # Split the value based on commas.
                # Dont strip surrounding quotes yet, since they are important
                # for guessing the data type
                value_list = self.split(value, stripnestingchar=False)

                # Guess the data type
                datatype = self.guess_data_type(value_list)
                if datatype is str:
                    # This is a string, so strip the surrounding quotes.
                    value_list = [self._strip_nesting_chars(x, self.list_nestchar) for x in value_list]
                else:
                    # Convert to the appropriate data type.
                    value_list = [datatype(x) for x in value_list]

            
                if len(value_list) == 1:
                    self[current_section][option] = value_list[0]
                else:
                    self[current_section][option] = value_list
                    
                continue
            
            # Could not parse this line.
            if strict:
                raise Exception('Line could not be parsed: {}'.format(line))

            

            

        
    
