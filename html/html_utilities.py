#!/usr/bin/env python
#
# Copyright 2009 Novimir Antoniuk Pablant
#
# html_utilites.py is free software: you can redistribute it and/or 
# modify it under the terms of the GNU General Public License as 
# published bythe Free Software Foundation, either version 2 of the 
# License, or (at your option) any later version.
#
# html_utilites.py is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with html_utilites.py.  
# If not, see <http://www.gnu.org/licenses/>.
#
# ======================================================================
# To Do:
#  - Make tag search case insensitive, or at least add the option.
#  - Add the option to use standard ssi syntax.
# ======================================================================
# Version Log:
# 2009-03-30 - 0.0.0 - Novimir Pablant
#                        Initial version
#
# 2009-04-22 - 0.0.1 - Novimir Pablant
#                        Fixed the way option are handled. 
#                        Added a 'source' option.
# 
# ======================================================================

from __future__ import print_function

import re
import sys, os, shutil
import warnings

from optparse import OptionParser
from filesearch import FileSearch

import datetime

__version__ = (0,0,1)
__version_date__ = datetime.date(2009,4,23)

# This is the main class.
class HtmlUtilities:
    def __init__(self):

        # Set defaults.
        self.eol_type = 'unix'

    def go(self, commands, **kwargs):

        self.commands = commands
        self.options = kwargs

        # Set the defaults
        self.options.setdefault('destination', "html_final")
        self.options.setdefault('source', ".")
        self.options.setdefault('files', "")
        self.options.setdefault('recursive', True)
        self.options.setdefault('simulation', False)
        self.options.setdefault('strict', False)
        self.options.setdefault('verbose', False)

        # Compile the regular expressions
        self.setupIncludeRegex()

        if self.commands:
            # If a string was passed, convert it to a list.
            if isinstance(self.commands, str):
                self.commands = [self.commands]

            if self.commands[0] == 'include':
                self.doInclude()
            else:
                pass

    def setupIncludeRegex(self):

        re_flags = 0
        if not self.options['strict']:
            re_flags = re.IGNORECASE

        # Match an include tag: <!-- <include tags="stuff" /> -->
        # Note that the <!-- --> is necessary for a match.
        if self.options['strict']:
            # Tags must be closed <include /> or <include></include>
            # Note: This will match <include /></include> which is not proper XHTML.
            self.regex_include_tag_string = \
                r"(?P<full_command>\<!--\s*\<include (?P<include_params>.*?)(/|/include.*?)/>\s*--\>)"
        else:
            # Unclosed tags will be allowed. <include>
            self.regex_include_tag_string = \
                r"(?P<full_command>\<!--\s*\<include (?P<include_params>.*?)\>\s*--\>)"


        self.regex_include_tag = re.compile(self.regex_include_tag_string, re_flags)

        
        self.regex_href_string = \
            "href\\s*=\\s*(?P<quote>\"|\')(?P<href>.*?)(?P=quote)"
        self.regex_href = re.compile(self.regex_href_string, re_flags)

        self.regex_tag_string = \
            "tag\\s*=\\s*(?P<quote>\"|\')(?P<tag>.*?)(?P=quote)"
        self.regex_tag = re.compile(self.regex_tag_string, re_flags)

    # ==================================================================
    # Here I have commands to include html into other html documents.
    # This is basically the same function as server side includes (SSI).
    #
    # For the time being I will use my own syntax, though I may decide
    # to use the SSI syntax at some point.
    #
    # For the time being I will scan all files and output any files
    # have have an <inculde> command.
    # 
    # Recursive commands will not be supported.
    def doInclude(self):

        print('Inserting includes', end='')
        if self.options['simulation']:
            print(' (simulation)', end='')
        print(':')

        # Loop over all the html files and add them to a list.
        # Note: I do this in two steps because if the output directory
        #       were to be somewhere below the search directory
        #       the new files could end up getting scaned.
        html_file_list = []
        for filename in FileSearch(self.options['source']
                                   ,pattern='*.html'
                                   ,recursive=self.options['recursive']):
            html_file_list.append(filename)

        # Loop over the filenames and do the replacement.
        for filename in html_file_list:
            self.replaceIncludes(filename)


        # Now copy all of the files in the given files directory.
        if self.options['files']:
            self.copytree(self.options['files'], self.options['destination']
                            ,ignore=shutil.ignore_patterns('*~', '.svn', 'CVS', '.bzr'))


    # ==================================================================
    # If I want recursive includes, the easest thing to do is to
    # set the search position to the begining of included html instead
    # of the end.  This however would make it very hard to keep track
    # of infitate loops.
    #
    # It would probably be better check for includes on
    # each file before including it. I could pass the include routine
    # a list of all the previous inculded files in the chain 
    # (make sure to convert to absolute path.  I can then check
    # any new includes against the list and prevent infinte loops.
    def replaceIncludes(self, filename):
        current_dir = os.path.dirname(filename)

        if self.options['verbose']:
            print('Scanning: {0}'.format(filename))

        # Open the file in binary . . .
        # I want to deal with end of line characters myself.
        with open(filename, 'rb') as file:
            # Read in the whole file.
            html_orig = file.read().decode()       

        html_new = html_orig

        include_found = False
        
        # Here we keep the location of the search in html_new
        search_position = 0
        # Here we keep track of the difference (in characters) 
        # between html_orig and html_new
        orig_match_offset = 0
        while True:
            match = self.regex_include_tag.search(html_new, search_position)

            # If no matches were found then we're done.
            if not match:
                break

            search_position = match.end('full_command')
            orig_match_start = match.start('full_command') - orig_match_offset

            # We found an include tag.  
            # Now we need to extract its parts.
            include_params = match.group('include_params')

            match_href = self.regex_href.search(include_params)
            if match_href:
                href = match_href.group('href')
                href = os.path.join(current_dir, href)
                href = os.path.normpath(href)
            else:
                href = None

            match_tag = self.regex_tag.search(include_params)

            if match_tag:
                tag = match_tag.group('tag')
            else:
                tag = None

            # The include tag must have a 'href'.
            if not href:
                linenumber = self.getLineNumber(html_orig, orig_match_start)
                print(MalformedTagWarning(filename
                                          ,'include'
                                          ,'No "href" attribute'
                                          ,linenumber=linenumber))
                continue

            # Open the include file.
            try:
                with open(href, 'rb') as file_include:
                    # Read in the whole file.
                    html_include = file_include.read().decode()
            except IOError:
                linenumber = self.getLineNumber(html_orig, orig_match_start)
                print(FileWarning(filename
                                  ,'Include file not found: "{0}"'.format(href)
                                  ,linenumber=linenumber))
                continue

            # We found a valid include statement.
            include_found = True
            
            # Extract the relevant portion from the include file.
            html_include_tag = self.extractTag(html_include, tag=tag)

            if html_include_tag == None:
                linenumber = self.getLineNumber(html_orig, orig_match_start)
                print(TagNotFoundWarning(
                        filename
                        ,tag
                        ,'Requested tag not found in include file: "{0}"'.format(href)
                        ,linenumber=linenumber))
                continue
                

            # Now replace the include statement with the include file.
            command_span = match.span('full_command')
            command_length = command_span[1]-command_span[0]

            include_length = len(html_include_tag)

            html_new = ''.join([html_new[:command_span[0]]
                                ,html_include_tag
                                ,html_new[command_span[1]:]])

            orig_match_offset += include_length - command_length
            search_position += include_length - command_length
        
        if include_found:
            filename_out = os.path.relpath(filename, self.options['source'])
            filename_out = os.path.join(self.options['destination'], filename_out)
            filename_out = os.path.normpath(filename_out)

            print(' Saved "{file_in}" to "{file_out}".'.format(file_in=filename
                                                               ,file_out=filename_out))

            if not self.options['simulation']:
                # Create the output directory structure if necessary.
                filename_out_path = os.path.dirname(filename_out)
                if not os.path.exists(filename_out_path):
                    os.makedirs(filename_out_path)

                with open(filename_out, 'wb') as file:
                    # Write out the file.
                    # Make sure the output uses a singel eol type.
                    file.write(self.changeEol(html_new, type=self.eol_type).encode())


    # ==================================================================
    # I need to save compliled patterns into a list.  This will save on
    # processing and improve speed.
    def extractTag(self, html, tag=None, id=None):
        
        if tag == None:
            return html
        else:
            regex_string = r"(?s)\<{tag}( [^>]*)?\>(?P<tag_content>.*?)\</{tag}\>".format(tag=tag)
            regex = re.compile(regex_string)

            match = regex.search(html)

            if match:
                return match.group('tag_content')
            else:
                return None


    def changeEol(self, text, type='unix'):
        """
        This will convert end of line characters between 
        dos (\r\n) and unix (\n) conventions.
        """

        if type == 'dos':
            (text, num_subs) = re.subn('(?<!\r)\n', '\r\n', text)
            if num_subs != 0:
                print('Converted {0} eols to dos.'.format(num_subs))                          
            return text
        elif type == 'unix':
            num_subs = text.count('\r\n')
            if num_subs != 0:
                print('Converted {0} eols to unix.'.format(num_subs))
            return text.replace('\r\n','\n')
        else:
            print('Type {0} unknown.'.format(type))

    def getLineNumber(self, string, position):
        return string.count('\n', 0, position) + 1


    def copytree(self, src, dst, symlinks=False, ignore=None):
        """Recursively copy a directory tree using copy2().

        If the destination directory already exists, files will be
        overwritten.

        If exception(s) occur, an Error is raised with a list of reasons.

        If the optional symlinks flag is true, symbolic links in the
        source tree result in symbolic links in the destination tree; if
        it is false, the contents of the files pointed to by symbolic
        links are copied.

        The optional ignore argument is a callable. If given, it
        is called with the `src` parameter, which is the directory
        being visited by copytree(), and `names` which is the list of
        `src` contents, as returned by os.listdir():

            callable(src, names) -> ignored_names

        Since copytree() is called recursively, the callable will be
        called once for each directory that is copied. It returns a
        list of names relative to the `src` directory that should
        not be copied.

        XXX Consider this example code rather than the ultimate tool.

        """
        names = os.listdir(src)
        if ignore is not None:
            ignored_names = ignore(src, names)
        else:
            ignored_names = set()


        if not os.path.exists(dst): os.makedirs(dst)

        errors = []
        for name in names:
            if name in ignored_names:
                continue
            srcname = os.path.join(src, name)
            dstname = os.path.join(dst, name)
            try:
                if symlinks and os.path.islink(srcname):
                    linkto = os.readlink(srcname)
                    os.symlink(linkto, dstname)
                elif os.path.isdir(srcname):
                    self.copytree(srcname, dstname, symlinks, ignore)
                else:
                    shutil.copy2(srcname, dstname)
                # XXX What about devices, sockets etc.?
            except (IOError, os.error) as why:
                errors.append((srcname, dstname, str(why)))
            # catch the Error from the recursive copytree so that we can
            # continue with other files
            except Error as err:
                errors.extend(err.args[0])
        try:
            shutil.copystat(src, dst)
        except OSError as why:
            if WindowsError is not None and isinstance(why, WindowsError):
                # Copying file access times may fail on Windows
                pass
            else:
                errors.extend((src, dst, str(why)))
        if errors:
            raise Error(errors)
      

# This class handles running from the command line.
class HtmlUtilitiesCommand(HtmlUtilities):
    def __init__(self):
        usage = """
  %prog command [options]

Commands:
  include               Replace <inculde /> tags with apropriate html.
"""

        # Warning:
        #  When using the command line the defaults that are set here.  
        #  When not using the command line the defaults are set by 
        #  HtmlUtilities.__init__().  Care should be taken to make sure
        #  that they are the same.
        
        # Setup the options parser.
        parser = OptionParser(usage) 
        parser.set_defaults(destination="html_final"
                            ,recursive=True)

        parser.add_option("-D", "--destination", metavar='DIR'
                          ,action="store", dest="destination"
                          ,help="The directory where the final files should be saved.") 

        parser.add_option("-S", "--source", metavar='DIR'
                          ,action="store", dest="source"
                          ,help="The directory to search for html files.")

        parser.add_option("-F", "--files", metavar='DIR'
                          ,action="store", dest="files"
                          ,help="Files in this directiory will be copied to the destination.")


        parser.add_option("-s", "--simulation"
                          ,action="store_true", dest="simulation"
                          ,help="Only do a simulation, do not write files.")

        parser.add_option("-n", "--non-recursive"
                          ,action="store_false", dest="recursive"
                          ,help="Search only the current directory.")

        parser.add_option("-t", "--strict"
                          ,action="store_true", dest="strict"
                          ,help="Strictly look for valid XHTML. This means that tags are case sensitive and must be closed.")

        parser.add_option("--verbose"
                          ,action="store_true", dest="verbose"
                          ,help="Print out extra stuff.")

        parser.add_option("-v", "--version"
                          ,action="store_true", dest="version"
                          ,help="Display the version.")


        (options, arg_list) = parser.parse_args()

        if options.version:
            print('html_utilities version {0} - {1}'.format(self.versionString()
                                                            ,self.versionDateString()))
            sys.exit()

        HtmlUtilities.__init__(self)
        self.go(arg_list, **options.__dict__)

    def versionString(self):
        string='.'.join(map(str,__version__[:3]))
        return string

    def versionDateString(self):
        return __version_date__.strftime('%Y-%m-%d')

#=======================================================================
#=======================================================================
# Setup errors and warnings
class Error(Exception):
    """Base class for exceptions in this module."""

    pass

class HtmlUtilitiesWarn:
    """Base class for exceptions in this module."""
    

class FileWarning(HtmlUtilitiesWarn):
    """Class for errors retated to a specific file."""

    def __init__(self, filename=None, message=None, linenumber=None):
        self.error_description = None
        self.filename = filename
        self.error_message = message
        self.linenumber = linenumber

    def __str__(self):
        message_list = ['   ']

        if self.error_description:
            message_list.append(self.error_description)
            
        if self.filename:
            msg_filename = 'File "{0}"'.format(self.filename)
            if self.linenumber:
                msg_filename += ', line {0}'.format(self.linenumber)

            message_list.append(msg_filename)

        if self.error_message:
            message_list.append(self.error_message)

        return self.formatwarning('\n    '.join(message_list))

    def formatwarning(self, message):

        return '\nWarning: {0}: {1}'.format(self.__class__.__name__, message)
                  
        
class SyntaxWarning(FileWarning):
    """General error for syntax errors while parsing files."""

    def __init__(self, *args):
        FileWarning.__init__(self, *args)
        self.error_description = 'Syntax error encountered while parsing file.'


class MalformedTagWarning(FileWarning):
    """Execption raised when a tag, or the information in it is not found."""

    def __init__(self, filename=None, tag=None, message=None, linenumber=None):
        FileWarning.__init__(self, filename, message, linenumber)

        if tag:       
            tag_sub = '<'+tag+'> '
        else:
            tag_sub = ''

        self.error_description = 'Malformed {0}tag encountered.'.format(tag_sub)

class TagNotFoundWarning(FileWarning):
    """Execption raised when a tag, or the information in it is not found."""

    def __init__(self, filename=None, tag=None, message=None, linenumber=None):
        FileWarning.__init__(self, filename, message, linenumber)

        if tag:       
            tag_sub = '<'+tag+'> '
        else:
            tag_sub = ''

        self.error_description = 'Tag not found: {0}'.format(tag_sub)
            

if __name__ == "__main__":
   html_util = HtmlUtilitiesCommand()
