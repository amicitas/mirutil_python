#!/usr/bin/env python
# -*- coding: utf-8 -*-

from optparse import OptionParser
import sys
import os
import fnmatch
import re

from mirutil.shell.filesearch import FileSearch
from mirutil.console import console


# ==============================================================================
# ==============================================================================
class SearchAndReplace():
   """
   A class that implements a regex search and replace.

   To Do
   -----
     I started writing a replaceFullText function which is currently unfinished.
     As part of this I should generalize readByLine so that I don't have
     to duplicate code betwen the two.  These two functions can probably
     be indentical except for a flag.
     
     Currently replacement does not work if a lookahead/lookbehind groups are
     used in the search pattern.
   """
    
   # ===========================================================================
   # ===========================================================================
   def __init__(self):
      usage = """%prog [options] search_pattern replace_pattern [file]

search_pattern will be interpreted as a regular expression.

file can either be a file or a directory to search.
If file is not given then search will begin at the current directory.
"""
      
      parser = OptionParser(usage)
      
      parser.set_defaults(verbose=False)
      parser.set_defaults(quiet=False)
      parser.set_defaults(include='')
      parser.set_defaults(ignore=None)
      parser.set_defaults(step_by_step=True)
      parser.set_defaults(force=False)
      parser.set_defaults(dryrun=False)
      parser.set_defaults(color=True)
      parser.set_defaults(include=[])
      parser.set_defaults(exclude=[])
      
      parser.add_option("-d", "--dry-run"
                        ,action="store_true", dest="dryrun"
                        ,help="Only simulate the subsitution. Do not change files.")
      
      parser.add_option("-s", "--skip-prompt"
                        ,action="store_false", dest="step_by_step"
                        ,help="No prompting for substitutions.")
      
      parser.add_option("--force"
                        ,action="store_true", dest="force"
                        ,help="Will run with no prompting whatsoever.")

      parser.add_option("-r", "--recursive"
                        ,action="store_true", dest="recursive"
                        ,help="Recursivly search directories.")
      
      parser.add_option('-i', '--case-ignore'
                        ,action="store_true", dest="ignore_case"
                        ,help="Ignore case distinctions in both the PATTERN and the input files.")
      
      parser.add_option('-I', '--include', metavar='PATTERN'
                        ,action="append", dest="include"
                        ,help="Replace only in files matching PATTERN.  This options may be used multiple times to include more than one pattern. For this option simplified glob matching is used (not regular expression matching).")
      
      parser.add_option('-E', '--exclude', metavar='PATTERN'
                        ,action="append", dest="exclude"
                        ,help="Exclude directories or files matching PATTERN. This options may be used multiple times to exclude more than one pattern.  For this option simplified glob matching is used (not regular expression matching).")
      
      parser.add_option('-c', '--no-color'
                        ,action="store_false", dest="color"
                        ,help="Do not colorize output.")

      parser.add_option("-v", "--verbose"
                        ,action="store_true", dest="verbose")
      
      parser.add_option("-q", "--quiet"
                        ,action="store_true", dest="quiet")


      # ------------------------------------------------------------
      parser.add_option("--tex"
                        ,action="store_true", dest="type_tex"
                        ,help="Replace in TeX files:  *.tex")
      parser.add_option("--idl", "--IDL"
                        ,action="store_true", dest="type_idl"
                        ,help="Replace in IDL files:  *.pro")
      parser.add_option("--python"
                        ,action="store_true", dest="type_python"
                        ,help="Replace in Python files:  *.py, *.pyx")
      parser.add_option("--fortran", "--FORTRAN"
                        ,action="store_true", dest="type_fortran"
                        ,help="Replace in Python files:  *.py, *.pyx")
      parser.add_option("--html"
                        ,action="store_true", dest="type_html"
                        ,help="Replace in HTML files:  *.html, *.htm")



      # ------------------------------------------------------------
      (self.options, arg_list) = parser.parse_args()

      self.args = {}
      
      if not len(arg_list) in (2,3):
         parser.error("incorrect number of arguments")
      elif len(arg_list) == 2 :
         self.args['path'] = '.'
      else:
         self.args['path'] = arg_list[2]
         
      self.args['search_pattern'] = arg_list[0]
      self.args['replace_pattern'] = arg_list[1]
 
      if self.options.quiet:
         self.options.verbose = False
        
      self.setupInclude()

      if not self.options.quiet:
         print("Replacing '%s' with '%s':" %(self.args['search_pattern'], self.args['replace_pattern']))
         
         if self.options.dryrun:
            print('(Dry run)')


      # If --force was set, then disable prompting.
      if self.options.force:
         self.options.step_by_step = False


      if (not self.options.step_by_step) and (not self.options.force):
         # Warn the user that no prompts will be given.
         print('Replacement will be done without prompting, continue?')
         question = input('yes (y), no (n) ? ')
         question = question.lower()

         # if no
         if question[0] == 'n':
            sys.exit('\ninterrupted by the user.') 
         
      # Get a console object for color output
      self.console = console.getConsole()



   # ===========================================================================
   # ===========================================================================
   def replace(self):
      "This is the main search and replace driver."
         

      # First check if the input was a file or directory.
      if os.path.isfile(self.args['path']):
         # Just do the replacement in the one file
         self.replaceByLine(self.args['path'])
         
      elif os.path.isdir(self.args['path']):
         # Recursively find files that match pattern
         for filename in FileSearch(self.args['path']
                                    ,pattern=self.options.include
                                    ,ignore=self.options.exclude
                                    ,file=True):
            self.replaceByLine(filename)
            
      else:
         print("No files/directories match '%s'." %(self.args['path']))



   # ===========================================================================
   # =========================================================================== 
   def compileSearchPattern(self):
      
      # Compile the search pattern.
      # Also compile the search pattern with parenthesis around the outside.
      #   This will allow me to reference the whole patten for colored output.
      
      if self.options.ignore_case:
         self.re_search = re.compile(self.args['search_pattern'], re.I)
      else:
         self.re_search = re.compile(self.args['search_pattern'])  

      
   # ===========================================================================
   # ===========================================================================               
   def replaceByLine(self, file):
      """
      Find the search pattern and replace with the replacement pattern.
      Do the search line by line.
      
      Input is a filename.

      One problem with this routine is that I don't necessarily know the
      encoding of the file that I am trying to read.
      For now I will assume utf-8, and make use of surrogate escape characters
      if I encounter a problem.  Even if the encoding is wrong, any ascii
      replacements will still work without corrupting the file.
      """

      self.compileSearchPattern()

      
      # Read in the file.
     
      # initialize the replace flag
      replaceflag = False
      
      # initialize the step_by_step flag to the global value
      step_by_step = self.options.step_by_step

      # open file for read
      # It is important to read and write in binary mode so that the
      # line endings don't get changed.
      readlines=open(file,'rb').readlines()
      
      # Decode to string type, assume utf-8.
      try:
         readlines = [line.decode('utf-8') for line in readlines] 
      except:
         print('Could not properly decode file using utf-8: '+file)
         readlines = [line.decode('utf-8', 'surrogateescape') for line in readlines] 
     
      # intialize the list counter
      listindex = -1

      # search and replace in current file printing to the user changed lines
      for currentline in readlines:

         # increment the list counter
         listindex = listindex + 1
         
         # See if there are any matches.
         match = self.re_search.search(currentline)
         if match:

            # Generate the replacement string.
            raw_replacement = self.re_search.sub(self.args['replace_pattern'], match.group(0))

            # Guess the case of the replacement string.
            replacement = self.guessReplacementCase(match.group(0), raw_replacement)
            
            # make the substitution
            newline = self.re_search.sub(replacement, currentline)

            if not self.options.quiet:
               # print the current filename, the old string and the new string
               print('\n{file}: {line:d}'.format(file=file, line=listindex+1))

               if self.options.color:
                  # make strings for color output
                  old_color = self.re_search.sub(''.join(['<console style="color:red;">'
                                                    ,match.group(0)
                                                    ,'</console>'])
                                           ,currentline)
                  new_color = self.re_search.sub(''.join(['<console style="color:red;">'
                                                     ,replacement
                                                     ,'</console>'])
                                            ,currentline)

                  self.console.printColor('< ' + old_color)
                  self.console.printColor('> ' + new_color)
               else:
                  print('< ' + currentline)
                  print('> ' + newline)

            
            # if this is a dryrun
            if self.options.dryrun:
               continue

            # if substitution is step by step
            if step_by_step:

               # ask user if the current line must be replaced
               while True:
                  question = input('Replace? yes (y), no (n), all (a), skip (s), no prompt (!), quit (q) ? ')
                  question = question.lower()
                  
                  if question in ('y','n','a','s','!','q'):
                     break


               # if skip
               if question == 'n':
                  continue
                  
               # if write all
               elif question == 'a':
                  step_by_step = False

               # if skip all
               elif question == 's':
                  return

               # if no prompt
               elif question == '!':
                  step_by_step = False
                  self.options.step_by_step = False

               # if quit
               elif question == 'q':
                  sys.exit('\ninterrupted by the user.')


            # update the whole file variable ('readlines')
            readlines[listindex] = newline
            replaceflag = True
                            
      # if some text was replaced
      # overwrite the original file
      if replaceflag:

         # open the file for writting  
         write_file=open(file,'wb') 

         # Encode back to bytes type.
         readlines = [line.encode('utf-8', 'surrogateescape') for line in readlines] 

         # overwrite the file  
         for line in readlines:
            write_file.write(line)

         # close the file
         write_file.close()

         
   # ===========================================================================
   # ===========================================================================               
   def replaceFullText(self, file):
      """
      Find the search pattern and replace with the replacement pattern.
      Do a full text search.
      
      Input is a filename.

      
      Programming Notes
      -----------------
        For now this copies a lot of code from :py:`replaceByLine`.  I should 
        break this into a bunch of sub funcitons so I don't have duplicate code.
      
        One problem with this routine is that I don't necessarily know the
        encoding of the file that I am trying to read.
        For now I will assume utf-8, and make use of surrogate escape characters
        if I encounter a problem.  Even if the encoding is wrong, any ascii
        replacements will still work without corrupting the file.
      """

      self.compileSearchPattern()

      
      # Read in the file.
     
      # initialize the replace flag
      replaceflag = False
      
      # initialize the step_by_step flag to the global value
      step_by_step = self.options.step_by_step

      # Read the file. Do this using binary mode to ensure that the
      # line ending don't get changed.
      with open(file, 'rb') as openfile:
         filetext = openfile.read()
          
      # Assume utf-8 for now.
      try:
         filetext = filetext.decode('utf-8')
      except:
         print('Could not properly decode file using utf-8: '+file)
         filetext = filetext.decode('utf-8', 'surrogateescape')

      raise NotImplementedError



         
   # ===========================================================================
   # ===========================================================================
   def printColor(self):
      pass
            

   # ===========================================================================
   # ===========================================================================
   def setupInclude(self):
      """Here we setup the correct include types."""
      self.options.include = []
      if self.options.type_tex:
         self.options.include.append('*.tex')
         
      if self.options.type_idl:
         self.options.include.append('*.pro')

      if self.options.type_python:
         self.options.include.append('*.py')
         self.options.include.append('*.pyx')

      if self.options.type_fortran:
         self.options.include.append('*.f')
         self.options.include.append('*.f90')

      if self.options.type_html:
         self.options.include.append('*.html')
         self.options.include.append('*.htm')

      # Finally set all files as the default.
      if not self.options.include:
         self.options.include.append('*')

                 
   # ===========================================================================
   # ===========================================================================
   def isSimpleSubstitution(self, original, new):
      return (self.args['search_pattern'].islower() and (original.isupper() or original.istitle()))


   # ===========================================================================
   # ===========================================================================
   def guessReplacementCase(self, original, new):
      """
      Match the case of the replacement text to the original text.

      The idea here is to (semi) intelegently match the case of the replacement
      text.  This is to partially match the behaviour of emacs with replacements.
      """

      if self.options.ignore_case:
         if self.isSimpleSubstitution(original, new):
            if original.isupper():
               return new.upper()
            elif original.istitle():
               return new.title()

      # By default return the replacement text unchanged.
      return new

# ==============================================================================
# ==============================================================================
if __name__ == "__main__":
   replacer = SearchAndReplace()
   replacer.replace()
