#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ==============================================================================
#
# Copyright 2009 Novimir Antoniuk Pablant
#
# filesearch.py is free software: you can redistribute it and/or 
# modify it under the terms of the GNU General Public License as 
# published bythe Free Software Foundation, either version 2 of the 
# License, or (at your option) any later version.
#
# filesearch.py is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with filesearch.py.  
# If not, see <http://www.gnu.org/licenses/>.
#
# ==============================================================================

import os
import fnmatch
import re

class FileSearch:
    """
    A forward interator that taverses a directory tree and returns 
    files that match a set of patterns.  
    Patterns should be separated by ','.
    """
    
    def __init__(self, directory
                 ,pattern="*"
                 ,ignore=None
                 ,file=False
                 ,dir=False
                 ,recursive=True
                 ,regex=False):


        # Ensure that pattern and ignore are lists.
        if isinstance(pattern, str):
            pattern = [pattern]
        if isinstance(ignore, str):
            ignore = [ignore]

        self.stack = [directory]
        self.pattern = pattern
        self.ignore = ignore
        self.recursive = recursive
        self.regex = regex

        self.files = []
        self.index = 0

        # Default is that if no keywords are passed then
        # both files are directories are returned
        if (not file) and (not dir):
            self.file = True
            self.dir = True
        else:
            self.file = file
            self.dir = dir



    def __getitem__(self, index):
        while 1:
            try:
                filename = self.files[self.index]
                self.index = self.index + 1
            except IndexError:
                # pop next directory from stack.
                # IndexError gets called when the stack is empty.
                self.directory = self.stack.pop()
                self.files = os.listdir(self.directory)
                self.index = 0
            else:
                # got a filename
                fullname = os.path.join(self.directory, filename)
                    
                # First check if this matches an ignore pattern.
                # Use regex instead of fnmatch to ensure that we are matching
                # against the full path, not just the filename.
                if self.ignore:
                    match = False
                    for ignore in self.ignore:
                        if self.regex:
                            re_pattern = ignore
                        else:
                            re_pattern = fnmatch.translate(ignore)
                        if re.match(re_pattern,fullname):
                            match = True
                            continue
                    # Continue while loop.
                    if match:
                        continue

                # If this is a directory, then add it to the stack.
                if os.path.isdir(fullname) and not os.path.islink(fullname):
                    if self.recursive:
                        self.stack.append(fullname)

                # Check if the file/dir matches the search pattern.
                match = False
                for pattern in self.pattern:
                    if self.regex:
                        re_pattern = pattern
                    else:
                        re_pattern = fnmatch.translate(pattern)
                    if re.match(re_pattern,fullname):
                        match = True
                        break

                # A match was found.
                if match:
                    if self.file and not self.dir:
                        if not os.path.isfile(fullname):
                            continue
                    elif self.dir and not self.file:
                        if not os.path.isdir(fullname):
                            continue
                        
                    return fullname
