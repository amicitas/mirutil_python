#!/usr/bin/env python

from optparse import OptionParser
import sys, os, shutil
import re

from filesearch import FileSearch

class SearchAndRename():

   def __init__(self):
      usage = """%prog [options] search_pattern replace_pattern [file]

This program will rename files by replacing the search_pattern with the 
replace_pattern.

search_pattern will be interpreted as a regular expression.

file can either be a file or a directory to search.
If file is not given then search will begin at the current directory.
"""
      
      parser = OptionParser(usage)
      
      parser.set_defaults(verbose=True)
      parser.set_defaults(include='*')
      parser.set_defaults(ignore=None)
      parser.set_defaults(step_by_step=True)
      parser.set_defaults(simulation=False)

      parser.add_option("-c", "--copy"
                        ,action="store_true", dest="copy"
                        ,help="Copy files instead of renaming.")  

      parser.add_option("-n", "--simulation"
                        ,action="store_true", dest="simulation"
                        ,help="Only simulate the subsitution. Do not change files.")  
      parser.add_option("-s", "--skip-prompt"
                        ,action="store_false", dest="step_by_step"
                        ,help="No prompting for substitutions.")              
      parser.add_option("-r", "--recursive"
                        ,action="store_true", dest="recursive"
                        ,help="Recursivly search directories.")
      parser.add_option('-i', '--case-ignore'
                        ,action="store_true", dest="ignore_case"
                        ,help="Ignore case distinctions in both the PATTERN and the input files.")
      
      
      parser.add_option('-I', '--include', metavar='PATTERN'
                        ,action="store", dest="include"
                        ,help="Search and rename only files matching PATTERN.  Multiple patterns may be separated by ','.")

      parser.add_option('-G', '--ignore', metavar='PATTERN'
                        ,action="store", dest="ignore"
                        ,help="Ignore directories or files matching PATTERN. Multiple patterns may be separated by ','.")

      
      parser.add_option("-v", "--verbose"
                        ,action="store_true", dest="verbose")
      parser.add_option("-q", "--quiet"
                        ,action="store_false", dest="verbose")
      
      (self.options, arg_list) = parser.parse_args()

      self.args = {}
      
      if not len(arg_list) in (2,3):
         parser.error("incorrect number of arguments")
      elif len(arg_list) == 2 :
         self.args['path'] = '.'
      else:
         self.args['path'] = arg_list[2]
         
      self.args['search_pattern'] = arg_list[0]
      self.args['replace_pattern'] = arg_list[1]
         
         
      if self.options.verbose:
         print("Replacing '%s' with '%s':" %(self.args['search_pattern'], self.args['replace_pattern']))
         
         if self.options.simulation:
            print('(simulation)')

   def rename(self):
      """This is the main search and rename driver."""
         

      # First check if the input was a file or directory.
      if os.path.isfile(self.args['path']):
         # Just do the replacement in the one file
         self.rename_file(self.args['path'])
         
      elif os.path.isdir(self.args['path']):
         # Recursively find files that match pattern
         for filename in FileSearch(self.args['path']
                                    ,pattern=self.options.include
                                    ,ignore=self.options.ignore
                                    ,file=True):
            self.rename_file(filename)
            
      else:
         print("No files/directories match '%s'." %(self.args['path']))

   def rename_file(self, filename):

      if self.options.ignore_case:
         re_search = re.compile(self.args['search_pattern'], re.I)
      else:
         re_search = re.compile(self.args['search_pattern'])  

      # initialize the step_by_step flag to the global value
      step_by_step = self.options.step_by_step

      headname, tailname = os.path.split(filename)
      
      if re_search.search(tailname):
         newtailname =  re_search.sub(self.args['replace_pattern'], tailname)
         newfilename = os.path.join(headname, newtailname)
         
         # print the current filename, the old string and the new string
         print('')
         print('< ' + filename)
         print('> ' + newfilename)

         # if this is a simulation
         if self.options.simulation:
            return

         # if substitution is step by step
         if step_by_step:

            # ask user if the current line must be replaced
            while True:
               question = input('write (y), skip (n), write all (!), quit (q) ? ')
               question = question.lower()

               if question in ('y','n','!','q'):
                  break

            # if quit
            if question == 'q':
               sys.exit('\ninterrupted by the user.')

            # if skip
            elif question == 'n':
               return
                  
            # if write all
            elif question == '!':
               step_by_step = False

         # Rename or copy, depending on the user options.
         if self.options.copy:
            shutil.copy(filename, newfilename)
         else:
            os.rename(filename, newfilename)

         
if __name__ == "__main__":
   renamer = SearchAndRename()
   renamer.rename()
