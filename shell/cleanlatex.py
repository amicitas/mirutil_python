#!/usr/bin/env python

from __future__ import print_function

from optparse import OptionParser
import filesearch
import os

class CleanLatex():
    def __init__(self):
        usage = """%prog [options]"""
        
        parser = OptionParser(usage)
      
        parser.set_defaults(simulation=False)

        parser.add_option("-n", "--simulation", "--dry-run"
                          ,action="store_true", dest="simulation"
                          ,help="Only simulate the cleanup. Do not remove files.")  
        parser.add_option("-a", "--all"
                          ,action="store_true", dest="remove_all"
                          ,help="Remove all generated files, including *.pdf and *.dvi.")
      
        (self.options, arg_list) = parser.parse_args()

    def cleanlatex(self, all=False):
        print('removing files', end='')
        if self.options.simulation:
            print(' (simulation)', end='')
        print(':')

        pattern=','.join(['*.4ct'
                          ,'*.4tc'
                          ,'*.aux'
                          ,'*.bbl'
                          ,'*.blg'
                          ,'*.dvi'
                          ,'*.glg'
                          ,'*.glo'
                          ,'*.glo_*'
                          ,'*.gls'
                          ,'*.gls_*'
                          ,'*.idv'
                          ,'*.idx'
                          ,'*.ilg'
                          ,'*.ind'
                          ,'*.ist'
                          ,'*.lg'
                          ,'*.lof'
                          ,'*.log'
                          ,'*.lot'
                          ,'*.out'
                          ,'*.tmp'
                          ,'*.toc'
                          ,'*.xref'
                          ])



        if self.options.remove_all:
            pattern += ',*.pdf,*.dvi'

        for filename in filesearch.FileSearch('.', pattern=pattern):
            print (filename)
            if not self.options.simulation:
                os.remove(filename)
        
if __name__ == "__main__":
    print("Cleaning up latex files.")
    cleaner = CleanLatex()
    cleaner.cleanlatex()
