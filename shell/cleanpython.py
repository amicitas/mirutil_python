#!/usr/bin/env python

from __future__ import print_function

from optparse import OptionParser
import filesearch
import os

class CleanPython():
    def __init__(self):
        usage = """%prog [options]"""
        
        parser = OptionParser(usage)
      
        parser.set_defaults(simulation=False)

        parser.add_option("-n", "--simulation"
                          ,action="store_true", dest="simulation"
                          ,help="Only simulate the cleanup. Do not remove files.")  
      
        (self.options, arg_list) = parser.parse_args()

    def cleanpython(self):
        print('removing files', end='')
        if self.options.simulation:
            print(' (simulation)', end='')
        print(':')

        for filename in filesearch.FileSearch('.', pattern='*.pyc,*.pyo,*~'):
            print (filename)
            if not self.options.simulation:
                os.remove(filename)
        
if __name__ == "__main__":
   cleaner = CleanPython()
   cleaner.cleanpython()
