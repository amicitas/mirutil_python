# -*- coding: utf-8 -*-
"""
author
------
  Novimir Antoniuk Pablant
    - npablant@pppl.gov
    - novimir.pablant@amicitas.com

"""
import subprocess

def rsync(source, destination, dryrun=False):
    """
    A simply wrapper of the linux rsync command line tool.

    returns:
      A CompletedProcess object from the subproccess call.
    """
    
    # Define the rsync command. 
    command = 'rsync -pvurt {} {}'
    if dryrun:
        command += ' --dry-run'

    command = command.format(source, destination)
    
    proc = subprocess.run(command, shell=True, capture_output=True)

    return proc

    
