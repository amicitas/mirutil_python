"""
Mir review!
"""

from collections import OrderedDict

from scipy import arange
from scipy.special import jn

from enable.api import ComponentEditor
from traits.api import HasTraits, Instance
from traitsui.api import Item, View

from chaco.api import VPlotContainer, Plot, ArrayPlotData
from chaco.pdf_graphics_context import PdfPlotGraphicsContext
from chaco.tools.api import PanTool, ZoomTool, LineInspector



## class PlotExample4(PlotExample3):
##     def _container_default(self):
##         container = super(PlotExample4, self)._container_default()

##         rplot, lplot = self.right_plot, self.left_plot
##         rplot.orientation = "v"
##         rplot.hgrid.mapper = rplot.index_mapper
##         rplot.vgrid.mapper = rplot.value_mapper
##         rplot.y_axis.mapper = rplot.index_mapper
##         rplot.x_axis.mapper = rplot.value_mapper


##         lplot.overlays.append(LineInspector(component=lplot,
##              axis="value", write_metadata=True, is_listener=True, color="blue"))
##         lplot.overlays.append(LineInspector(component=lplot,
##              axis="value", write_metadata=True, is_listener=True, color="blue"))

##         rplot.overlays.append(LineInspector(component=rplot,
##              axis="value", write_metadata=True, is_listener=True, color="blue"))
##         rplot.overlays.append(LineInspector(component=rplot,
##              axis="value", write_metadata=True, is_listener=True, color="blue"))

##         return container


## class PlotExample3(PlotExample2):
##     def _container_default(self):
##         container = super(PlotExample3, self)._container_default()

##         rplot, lplot = self.right_plot, self.left_plot
##         lplot.overlays.append(LineInspector(component=lplot,
##                 write_metadata=True, is_listener=True))
##         rplot.overlays.append(LineInspector(component=rplot,
##                 write_metadata=True, is_listener=True))
##         rplot.index = lplot.index

##         return container
    
## class PlotExample2(PlotExample):
##     def _container_default(self):
##         container = super(PlotExample2, self)._container_default()

##         rplot, lplot = self.right_plot, self.left_plot
##         rplot.index_mapper.range = lplot.index_mapper.range
##         rplot.value_mapper.range = lplot.value_mapper.range

##         lplot.overlays.append(ZoomTool(lplot, tool_mode="box",always_on=False))
##         rplot.overlays.append(ZoomTool(rplot, tool_mode="box",always_on=False))




class MirReview(HasTraits):
    container = Instance(VPlotContainer)

    traits_view = View(Item('container'
                            ,editor=ComponentEditor()
                            ,show_label=False
                            ,resizable=True)
                       ,width=800
                       ,height=600
                       ,resizable=True
                       ,title="Mir Review")


    data = ArrayPlotData()
    plot_dict = OrderedDict()

    
    def _container_default(self):
        container = VPlotContainer(spacing=5
                                   ,padding=5
                                   ,bgcolor="lightgray")
        return container


    def _container_changed(self):
        print('yo')


    def addData(self, *args, **kwargs):
        return self.data.set_data(*args, **kwargs)

        
    def addToPlot(self
                  ,name=None
                  ,type='line'
                  ,value=None
                  ,**kwargs):


        # Combine the input an the keywords into a single dictionary.
        if value is None:
            value = kwargs
        else:
            value.update(kwargs)
        
        # Update the name for this plot.
        # Generate the name using the plot index if needed.
        if name is not None:
            value['name'] = name
        elif ~ value.has_key('name'):
            value['name'] = len(self.plot_dict.len)


        # Get the plot by name or create a new plot.
        try:
            plot = self.plot_dict[value['name']]
        except KeyError:
            plot = self.addNewPlot(value['name'])


        # Add the data if it is new.
        data_names = []
        for data in value['data']:
            if isinstance(data, str):
                data_names.append(data)
            else:
                name = self.addData('', data, generate_name=True)
                data_names.append(name)

        value['data'] = data_names

        # Overplot the new data.
        plot.plot(type=type, **value)
        if value.has_key('line_width'):
            value['line_width'] = 1.0
        plot.plot(type="scatter", marker_size=1, **value)

        # Parse global keywords.
        if value.has_key('title'):
            plot.title = value['title']
            
        if value.has_key('x_title'):
            plot.index_axis.title = value['x_title']

        if value.has_key('y_title'):
            plot.value_axis.title = value['y_title']

        self.container.request_redraw()

        return plot
        
    def addNewPlot(self, name):
        plot = Plot(self.data)
        plot.tools.append(PanTool(plot))

        plot.padding_left = 50
        plot.padding_right = 5
        plot.padding_top = 5
        plot.padding_bottom = 20
        
        self.plot_dict[name] = plot
        
        # Add the plot to the plot container.
        self.container.add(plot)

        return plot


    def show(self):
        self.configure_traits()

    def save(self, plot, filename=None):

        gc = PdfPlotGraphicsContext(filename=filename)
        gc.render_component(plot)

        gc.save()


demo = MirReview()

if __name__ == "__main__":
    
    demo.show()
    demo.addDemoData()
