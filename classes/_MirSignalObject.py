# -*- coding: utf-8 -*-
"""
author
------
  Novimir Antoniuk Pablant
    - npablant@pppl.gov
    - novimir.pablant@amicitas.com

purpose
-------
  A standard object to use for handling diagnostic signals.
"""

import numpy as np

from mirutil import logging

from mirutil.classes._MirOrderedDict import MirOrderedDict

class MirSignalObject(MirOrderedDict):

    def __init__(self, dict_in=None):
        super().__init__()

        self['dim'] = MirOrderedDict()
        self['coord'] = MirOrderedDict()
        self['value'] = MirOrderedDict()
        self['sigma'] = MirOrderedDict()
        self['mask'] = MirOrderedDict()
        self['units'] = MirOrderedDict()
        self['info'] = MirOrderedDict()

        if dict_in is not None:
            for key in dict_in:
                if isinstance(dict_in[key], dict):
                    if not key in self:
                        self[key] = MirOrderedDict()
                    self[key].update(dict_in[key])
                else:
                    self[key] = dict_in[key]


    def update(self, input_dict):
        """
        Recursively update all entries of the signal object.
        """
        for name in input_dict:
            if name in self:
                self[name].update(input_dict[name])
            else:
                self[name] = input_dict[name]


    def createMask(self):
        """
        Create a basic signal mask based on finite values.

        At the moment this uses three criteria:
          1. 'value' is finite
          2. 'sigma' is finite
          3. 'sigma' is non-zero positive.

        ToDo
        ----
          In the future I may want to add some options on which of these checks
          to perform.
        """
        
        for key in self['value']:
            if not key in self['mask']:
                self['mask'][key] = np.ones(self['value'][key].shape, dtype=bool)

        for key in self['value']:
            self['mask'][key][:] &= np.isfinite(self['value'][key])
            if key in self['sigma']:
                self['mask'][key][:] &= np.isfinite(self['sigma'][key])
                self['mask'][key][:] &= self['sigma'][key] > 0
                

    def convertTimeseriesTo2d(self):

        num_dims = len(self['dim'])
        if num_dims > 2:
            raise Exception('Only 1 or 2 dimensional data is currently supported.')
        
        # If one dimensional data is found, then expand everything into two
        # dimensions. This currently only works if the given dimension
        # is a time dimension.
        if num_dims == 1:
            if 'time' in self['dim'] or 'timestamp' in self['dim']:
                self['dim']['channel'] = np.ones(1)
                for head_key in ['coord', 'value', 'sigma']:
                    for key in self[head_key]:
                        if self[head_key][key].ndim == 1:
                            self[head_key][key] = np.expand_dims(self[head_key][key], axis=1)                  
            else:
                raise Exception('Cannot automatically expand to 2 dimensions.')
            

    def _expandArrays(self, key):
        """
        Expand any arrays that have truncated dimensions.

        In the interest of avoiding duplication, and thereby saving computational
        time and memory, I have allowed redundant coordinates to be truncated in 
        the signal structure.  

        This works ok, but can complicate use of these signal structures if they
        are used outside of stelltran.

        This method will expand all coordinates into to full 2D arrays.
        """

        num_time = len(self['dim'][0])
        num_space = len(self['dim'][1])
            
        # If the radial coordinate is already a two dimentional coordinate, then
        # assume that the format is correct and don't do anything.
        for name in self[key]:
            if not self[key][name].shape[0:2] == (num_time, num_space):
                if self[key][name].ndim == 2:
                    shape = self[key][name].shape
                    new = np.zeros((num_time, num_space), dtype=self[key][name].dtype)
                    if shape[0] == 1:
                        for ii_space in range(num_space):
                            new[:, ii_space] = self[key][name][0, ii_space]
                    if shape[1] == 1:
                        for ii_time in range(num_time):
                            new[ii_time, :] = self[key][name][ii_time, 0]

                    #from IPython import embed
                    #embed()
                    
                elif self[key][name].ndim == 3:
                    new = np.zeros((num_time, num_space, self[key][name].shape[2]), dtype=self[key][name].dtype)
                    for ii_time in range(num_time):
                        for ii_r in range(num_space):
                            new[ii_time, ii_r, :] = self[key][name][ii_time, 0, :]
                else:
                    raise Exception('{} {} has an unexpected ndim: {}'.format(key, name, self[key][name].ndim))
                
                self[key][name] = new

                
    def expandCoordinates(self):
        self._expandArrays('coord')

        
    def expandAllArrays(self):
        self._expandArrays('coord')
        self._expandArrays('value')
        self._expandArrays('sigma')
        self._expandArrays('mask')

        
    def truncateToTimerange(self, signal, time_range):
        """
        Truncate the signal within a profile signal dict to the given time range.
        This is usefull both for excluding bad signal and reducing computation
        time.
        """
        
        if time_range:
            
            time_mask=(
                (self['coord']['time'][:,0] >= time_range[0])
                & (self['coord']['time'][:,0] <= time_range[1]))
        
            if not True in time_mask:
                self.log.exception('time_range: {}'.format(time_range))
                self.log.exception('signal_times: {}'.format(self['dim']['time']))
                raise Exception('No signal within given timerange.')
            
            self['dim'][0] = self['dim'][0][time_mask]
                        
            for key in self['coord']:
                if self['coord'][key].shape[0] > 1:
                    self['coord'][key] = self['coord'][key][time_mask,...]
                
            for key in self['value']:
                self['value'][key] = self['value'][key][time_mask,...]

            for key in self['sigma']:
                self['sigma'][key] = self['sigma'][key][time_mask,...]

            for key in self['mask']:
                self['mask'][key] = self['mask'][key][time_mask,...]
                
        return self


    def extractSingleTime(self, time_request):
            
        ii_time = np.argmin(np.abs(self['coord']['time'][:,0] - time_request))
        # Create a slice object so that we can retain the first dimension.
        ss_time = slice(ii_time,ii_time+1)
        
        output = self.__class__()


        for ii, key in enumerate(self['dim']):
            if ii == 0:
                output['dim'][key] = self['dim'][key][ss_time].copy()
            else:
                output['dim'][key] = self['dim'][key].copy()
            
        for name in self['coord']:
            if self['coord'][name].shape[0] == 1:
                ss_time_clps = slice(0,1)
            else:
                ss_time_clps = ss_time                
            output['coord'][name] = self['coord'][name][ss_time_clps].copy()

        for name in self['value']:
            output['value'][name] = self['value'][name][ss_time].copy()
            output['sigma'][name] = self['sigma'][name][ss_time].copy()

        for name in self['mask']:
            output['mask'][name] = self['mask'][name][ss_time].copy()

        output['units'].update(self['units'])            
        output['info'].update(self['info'])

        output['info']['time_request'] = time_request
        
        return output


