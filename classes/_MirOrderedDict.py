#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Provides a dictionary indexed by insertion order.
# Copyright (c) 2013 Niklas Fiekas <niklas.fiekas@tu-clausthal.de>
# Copyright (c) 2018 Novimir Pablant <novimir.pablant@amicitas.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
package stelltran

authors
-------
  Niklas Fiekas
    - niklas.fiekas@tu-clausthal.de
  Novimir Antoniuk Pablant
    - novimir.pablant@amicitas.com

description
-----------
  A modified OrderedDictionary object that include lookup by index.

performance
-----------
  Performance of this object for standard dict operations is very poor compared 
  to OrderedDict. The main cause of the poor performance is simply that
  basic methods are now implemented in python.

  Tested on 2018-09-09, 10000 values

  access_for_key_in:
    OrderedDict     Total: 0.0248s, Single: 2.4788us
    MirOrderedDict  Total: 0.0256s, Single: 2.5641us
  
  access_by_key:
    OrderedDict     Total: 0.0417s, Single: 4.1671us
    MirOrderedDict  Total: 0.4860s, Single: 48.6023us
  
  access_by_index:
    MirOrderedDict  Total: 1.4996s, Single: 149.9563us
  
  access_keys:
    OrderedDict     Total: 0.0248s, Single: 2.4781us
    MirOrderedDict  Total: 0.0577s, Single: 5.7723us
  
  access_values:
    OrderedDict     Total: 0.0370s, Single: 3.6969us
    MirOrderedDict  Total: 0.5533s, Single: 55.3293us
  
  access_items:
    OrderedDict     Total: 0.0412s, Single: 4.1164us
    MirOrderedDict  Total: 0.5726s, Single: 57.2584us
"""

from collections import OrderedDict
from collections import KeysView, ValuesView, ItemsView

class MirOrderedDict(OrderedDict):
    """
    A hybrid OrderedDict and List object.
    """

    
    def __init__(self, *args, **kwargs):
        """
        Initialize an ordered dictionary.
        The signature is the same as regular dictioneries, but keywords 
        arguments are not recommended because their insertion order is arbitrary.
        """
        self._map = []
        super().__init__(*args, **kwargs)


    def __setitem__(self, key_in, value_in):
        """dict.__setitem__(key, value) <==> dict[key] = value"""
        if isinstance(key_in, str):
            if key_in not in self:
                self._map.append(key_in)
            super().__setitem__(key_in, value_in)
        elif isinstance(key_in, int):
            super().__setitem__(self.keys()[key_in], value_in)
        elif isinstance(key_in, slice):
            keyarr = list(self.keys())[key_in]
            for ii in range(len(keyarr)):
                super().__setitem__(keyarr[ii], value_in[ii])
        else:
            raise TypeError('Only strings are allowed as keys for an MirOrderedDict')

        
    def __getitem__(self, key_in):
        """value = dict.__getitem__(key) <==> value = dict[key]"""
        if isinstance(key_in, str):
            return super().__getitem__(key_in)
        elif isinstance(key_in, int):
            return self.values()[key_in]
        elif isinstance(key_in, slice):
            return self.values()[key_in]
        elif isinstance(key_in, list) or isinstance(key_in, tuple):
            return [self[xx] for xx in key_in]
        else:
            raise TypeError('Only strings are allowed as keys for an MirOrderedDict')

        
        
    def __delitem__(self, key, dict_delitem=dict.__delitem__):
        """dict.__delitem__(key) <==> del dict[key]"""
        dict_delitem(self, key)
        self._map.remove(key)


    def clear(self):
        """Remove all items from dict."""
        self._map[:] = []
        super().clear()

        
    def pop(self, key=None, *args):
        """
        If key is in the dictionary, remove it and return its value, else return
        default. If default is not given and key is not in the dictionary, a 
        KeyError is raised.
        """
        if key is None:
            index = 0
        else:
            index = self._map.index(key)
        self._map.pop(index)
        return super().pop(key, *args)

    
    def popitem(self):
        """
        Return and remove a (key, value) pair. Pairs are returned LIFO order.
        """
        key = self._map.pop()
        return super().popitem()

    
    def keys(self):
        return IndexedKeysView(self)

    
    def values(self):
        return IndexedValuesView(self)

    
    def items(self):
        return IndexedItemsView(self)

    
class IndexedKeysView(KeysView):

    def __getitem__(self, index):
        return  self._mapping._map[index]

    def index(self, key):
        return  self._mapping._map.index([key])

    def __str__(self):
        return list(self._mapping.keys()).__str__()

    def __repr__(self):
        return list(self._mapping.keys()).__str__()
    
    
class IndexedValuesView(ValuesView):

    def __getitem__(self, index):
        return self._mapping[self._mapping._map[index]]

    def __str__(self):
        return list(self._mapping.values()).__str__()

    def __repr__(self):
        return list(self._mapping.values()).__str__()

    
class IndexedItemsView(ItemsView):

    def __getitem__(self, index):
        return key, self._mapping[self._mapping._map[index]]

    def __str__(self):
        return list(self._mapping.items()).__str__()

    def __repr__(self):
        return list(self._mapping.items()).__str__()
