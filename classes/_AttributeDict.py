

class AttributeDict(dict):
    """This is a dictionary where the entries can be accessed via the dot operator."""
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__
    
