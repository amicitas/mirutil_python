# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
"""
Authors:
  | Novimir Antoniuk Pablant <npablant@pppl.gov>

Purpose:
  Create a standard set of loggers for use in mir projects.

"""
# ------------------------------------------------------------------------------


from collections import OrderedDict

import os
import time
import logging
import logging.config
from logging import *
import re

from mirutil import console


class MirConsoleHandler(logging.StreamHandler):

    # For some reason this seems to be broken in for python 2.7.10.
    # I sohuld remove this once it is fixed.
    terminator = '\n'
    
    def __init__(self, *args, **kwargs):
        logging.StreamHandler.__init__(self, *args, **kwargs)

        self.console = console.getConsole()

    
    def _addColor(self, text):

        text = re.sub(
            r'(error)'
            ,r'<console style="color:red">\1</console>'
            ,text
            ,flags=re.IGNORECASE)

        text = re.sub(
            r'(except[ion]*)'
            ,r'<console style="color:red">\1</console>'
            ,text
            ,flags=re.IGNORECASE)

        text = re.sub(
            r'(warn[ing]*)'
            ,r'<console style="color:darkred">\1</console>'
            ,text
            ,flags=re.IGNORECASE)

        text = re.sub(
            r'(info)'
            ,r'<console style="color:darkgrey">\1</console>'
            ,text
            ,flags=re.IGNORECASE)

        text = re.sub(
            r'(debug)'
            ,r'<console style="color:black">\1</console>'
            ,text
            ,flags=re.IGNORECASE)

        return text


    def emit(self, record):
        """
        Convert HTML colors to ansi, otherwise this is a copy of the standard
        StreamHandler.event method.

        Emit a record.

        If a formatter is specified, it is used to format the record.
        The record is then written to the stream with a trailing newline.  If
        exception information is present, it is formatted using
        traceback.print_exception and appended to the stream.  If the stream
        has an 'encoding' attribute, it is used to determine how to do the
        output to the stream.
        """

        try:
            msg = self.format(record)

            # Add HTML color tags, then convert to ANSI escape sequences.
            msg = self._addColor(msg)
            msg = self.console.htmlToAnsi(msg)

            stream = self.stream
            stream.write(msg)
            stream.write(self.terminator)
            self.flush()
        except Exception:
            self.handleError(record)


class MirTimedRotatingFileHandler(logging.handlers.TimedRotatingFileHandler):
    """
    Handler for logging to a file, rotating the log file at certain timed
    intervals. 

    This is different to TimedRotatingFileHandler in that it always
    logs to a filename based on the timing information, and creates a new file
    at every rollover.  Also by default this will append to existing files
    rather than overwrite.

    If backupCount is > 0, when rollover is done, no more than backupCount
    files are kept - the oldest ones are deleted.
    """
        
    def __init__(self, filename, **kwargs):
        logging.handlers.TimedRotatingFileHandler.__init__(self, filename, delay=True, **kwargs)
        
        self.origFilename = filename
        self.doRollover()

        
    def _generateFilename(self):
        """
        Generate the filename for the current date.
        """
        # get the time that this sequence started at and make it a TimeTuple
        t = self.rolloverAt - self.interval
        
        if self.utc:
            timeTuple = time.gmtime(t)
        else:
            timeTuple = time.localtime(t)
            
        filename = self.origFilename + "." + time.strftime(self.suffix, timeTuple)

        return filename

    
    def doRollover(self):
        """
        This is a reimplimentation of the doRollover method.
        """
        if self.stream:
            self.stream.close()
        filename_new = self._generateFilename()
        #if os.path.exists(filename_new):
        #    os.remove(filename_new)
        if self.backupCount > 0:
            # find the oldest log file and delete it
            #s = glob.glob(self.baseFilename + ".20*")
            #if len(s) > self.backupCount:
            #    s.sort()
            #    os.remove(s[0])
            for s in self.getFilesToDelete():
                os.remove(s)
        #print "%s -> %s" % (self.baseFilename, filename_new)
        self.baseFilename = filename_new
        self.mode = 'a'
        self.stream = self._open()
        newRolloverAt = self.rolloverAt + self.interval
        currentTime = int(time.time())
        while newRolloverAt <= currentTime:
            newRolloverAt = newRolloverAt + self.interval
        #If DST changes and midnight or weekly rollover, adjust for this.
        if (self.when == 'MIDNIGHT' or self.when.startswith('W')) and not self.utc:
            dstNow = time.localtime(currentTime)[-1]
            dstAtRollover = time.localtime(newRolloverAt)[-1]
            if dstNow != dstAtRollover:
                if not dstNow:  # DST kicks in before next rollover, so we need to deduct an hour
                    newRolloverAt = newRolloverAt - 3600
                else:           # DST bows out before next rollover, so we need to add an hour
                    newRolloverAt = newRolloverAt + 3600
        self.rolloverAt = newRolloverAt


def checkAndCreateDirectory(path_in):
    path = os.path.abspath(os.path.dirname(path_in))
    if not os.path.exists(path):
        os.makedirs(path)
        
        logging.info('Created directory for log files: {}'.format(path))


# =============================================================================
# Add some new logging levels.

def debugverbose(self, msg, *args, **kwargs):
    """
    Log 'msg % args' with severity 'DEBUGVERBOSE'.

    To pass exception information, use the keyword argument exc_info with
    a true value, e.g.

    logger.debugvebose("Houston, we have a %s", "thorny problem", exc_info=1)
    """
    if self.isEnabledFor(logging.DEBUGVERBOSE):
        self._log(logging.DEBUGVERBOSE, msg, args, **kwargs)


def addLoggingLevels():
    logging.DEBUGVERBOSE = 5
    logging.addLevelName(logging.DEBUGVERBOSE, 'DEBUGVERBOSE')
    logging.Logger.debugverbose = debugverbose


# =============================================================================
# Define routines to add handlers.

def addColorConsoleHandler(level=None):
    if level is None: level = logging.DEBUG
    
    # First create a console logger     
    format_console = '[%(asctime)s %(levelname)-7.07s:%(name)-15.015s]  %(message)s'
    datefmt_console = '%H:%M:%S'
    formatter_console = logging.Formatter(format_console, datefmt_console)
    handler_console = MirConsoleHandler()
    handler_console.setFormatter(formatter_console)
    handler_console.setLevel(level)
    logging.root.addHandler(handler_console)

    return handler_console

    
def addRotatingFileHandler(filepath_in, level=None):
    if level is None: level = logging.DEBUG
    
    # Now add a file logger
    filepath = os.path.abspath(filepath_in)
    checkAndCreateDirectory(filepath)
    
    format_file = '[%(asctime)s %(levelname)-7.07s:%(name)-25.025s]  %(message)s'
    datefmt_file = '%Y-%m-%d %H:%M:%S'
    formatter_file = logging.Formatter(format_file, datefmt_file)
    handler_file = MirTimedRotatingFileHandler(filepath, when='midnight')
    handler_file.setFormatter(formatter_file)
    handler_file.setLevel(level)
    logging.root.addHandler(handler_file)

    return handler_file


def getLogger(*args, **kwargs):
    if not args:
        return logger_root
    else:
        return logger_root.getChild(*args, **kwargs)


# Add the new logging levels.
addLoggingLevels()

logger_root = logging.getLogger()

# Now create a default logger which includes a ColorConsole Handler.
if not logging.root.hasHandlers():
    addColorConsoleHandler()
    logger_root.setLevel(logging.DEBUG)
    
logger_root.info('Finished import of mirutil.logging.')
