#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================

"""
\package xcrystal

\author
   Novimir Antoniuk Pablant
     - npablant@pppl.gov
     - novimir.pablant@amicitas.com

\brief
   Convert IDL *.PRO files into Python *.py files.

\detailed
   This module will do simple parsing of IDL *.pro files into *.py files.
   This is not meant to be a full translation, but rather a way to simplify
   manual conversion.

   I have a particular and consistant coding style in IDL that simplifies
   this translation task.  This code will likely not work for anyone eleses
   code.

"""

import re

def simpleReplace(input_text):

    # Make a copy of the input text.
    text = input_text


    subs = []

    subs.append((r'^ *function +(\w+::)?(\w+), *(.*)(?<!\$)( *\n)', r'def \2(\3):\n', re.I|re.M))
    subs.append((r'^ *pro +(\w+::)?(\w+), *(.*)(?<!\$)( *\n)', r'def \2(\3):\n', re.I|re.M))


    # Control sequence replacement.
    subs.append((r'return *,', r'return', re.I))
      
    subs.append((r'(?<![\w.])for (\w+)=(\w+),(.+?)(-1)? do begin', r'for \1 in range(\2, \3):', re.I))
    subs.append(('endfor', '', re.I))

    subs.append((r'(?<![\w.])if (.+?) then( begin)?', r'if \1:', re.I))
    subs.append((r'(?<![\w.])else( begin)?', r'else:', re.I))
    
    subs.append((r'endif *', r'', re.I))
    subs.append((r'endelse *', r'', re.I))


    # Symbol replacement.
    subs.append((r'\^', r'**', re.I))
    subs.append((r' eq ', r' == ', re.I))
    subs.append((r' ne ', r' =! ', re.I))
    subs.append((r' gt ', r' > ', re.I))
    subs.append((r' ge ', r' >= ', re.I))
    subs.append((r' lt ', r' < ', re.I))
    subs.append((r' le ', r' <= ', re.I))


    # Function replacement.
    subs.append((r'(?<![\w.])size\((.*), */DIM\w*\)', r'np.shape(\1)', re.I))
    subs.append((r'(?<![\w.])n_elements\(', r'np.size(', re.I))
    subs.append((r'(?<![\w.])min\(', r'np.amin(', re.I))
    subs.append((r'(?<![\w.])where\(', r'np.where(', re.I))
    subs.append((r'(?<![\w.])sort\(', r'np.argsort(', re.I))
    subs.append((r'(?<![\w.])shift\(', r'np.roll(', re.I))
    subs.append((r'(?<![\w.])abs\(', r'np.abs(', re.I))
        
    subs.append((r'(?<![\w.])dblarr\(', r'np.zeros(', re.I))
    subs.append((r'(?<![\w.])fltarr\(', r'np.zeros(', re.I))

    
    # IDL directives.
    subs.append((r'^ *compile_opt.*?$', r'', re.I|re.M))


    # IDL lexical symbols.
    subs.append((r'^([^;]*);', r'\1#', re.I|re.M))
    
    # Make sure to do this one last, since I need to be able to search for
    # continuation characters in the above expressions.
    subs.append((r' *\$\n', r'\n', re.I))

        
    for sub in subs:
        text = re.sub(sub[0], sub[1], text, flags=sub[2])
    

    return text


def parseInPlace(filename, dryrun=False):
    f = open(filename, 'r')
    orig = f.read()
    f.close()
    
    new = simpleReplace(orig)

    if dryrun:
        print(new)
    else:
        f = open(filename, 'w')
        f.write(new)
        f.close()
